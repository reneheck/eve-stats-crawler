/**
 * Created by cerias on 06.02.15.
 */
var crawlerApp = angular.module('crawlerApp', [])


crawlerApp.filter('slice', function() {
        return function(arr, start, end) {
            return (arr || []).slice(start, end);
        };
    });