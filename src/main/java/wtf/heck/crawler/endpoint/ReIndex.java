package wtf.heck.crawler.endpoint;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.count.CountRequestBuilder;
import org.elasticsearch.action.count.CountResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.OrFilterBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.LoggerFactory;
import wtf.heck.elasticsearch.ElasticSearchClient;

import wtf.heck.elasticsearch.ElasticSearchCluster;
import wtf.heck.eve.data.EveItem;
import wtf.heck.stats.killmail.Killmail;

import wtf.heck.eve.zkillboard.json.Zkilloard;
import wtf.heck.eve.zkillboard.json.ZkilloardReady;
import wtf.heck.stats.killmail.KillmailFactory;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.*;
import java.util.logging.Logger;

/**
 * Created by cerias on 28.01.15.
 */
@Path("es")
public class ReIndex {
    org.slf4j.Logger log = LoggerFactory.getLogger(ReIndex.class);
    @Inject
    ElasticSearchClient esc;
    @Inject
    ElasticSearchCluster cluster;
    ObjectMapper mapper = new ObjectMapper();


    @Path("transfer/{size}")
    @GET
    public Response transfer(@PathParam("size")int size) throws IOException {

        final long iVersion = 4;

        String source = "zkillboard";

        ObjectMapper mapper = new ObjectMapper();
        KillmailFactory factory = new KillmailFactory();


        SearchRequestBuilder rein = esc.getClient().prepareSearch(esc.getINDEX());
        rein.setTypes(source);
        OrFilterBuilder orFilter = new OrFilterBuilder();
        orFilter.add(FilterBuilders.rangeFilter("iVersion").gt(iVersion));
        orFilter.add(FilterBuilders.notFilter(FilterBuilders.existsFilter("iVersion")));
        rein.setPostFilter(orFilter);
        rein.setSize(size);

        BulkRequestBuilder bulkRequestClient = esc.getClient().prepareBulk();
        BulkRequestBuilder bulkRequestCluster = cluster.getClient().prepareBulk();

        SearchResponse response =rein.execute().actionGet();
        log.info("Updating: "+response.getHits().getHits().length + "|from: "+ response.getHits().getTotalHits());

        for(SearchHit hit:response.getHits()){
            if(hit.getSourceAsString()!=null) {
                ZkilloardReady kill = mapper.readValue(hit.getSourceAsString(), ZkilloardReady.class);


                Killmail km =factory.fromZkillboard(kill, iVersion, esc);


                kill.setiVersion(iVersion);
                bulkRequestClient.add(esc.updateFieldBulk(source,kill.getKillID()+"","iVersion",kill.getiVersion()));

            }
        }

        if(bulkRequestClient.numberOfActions()>0) {
            BulkResponse responses = bulkRequestClient.execute().actionGet();

            if (responses.hasFailures()) {
                log.error(responses.buildFailureMessage());
            }
        }





        return Response.ok("done", MediaType.TEXT_PLAIN_TYPE).build();
    }



    @Path("reindex")
    @GET
    public Response reindex() throws IOException {

        SearchRequestBuilder rein = esc.getClient().prepareSearch(esc.getINDEX());
        rein.setTypes("zkillboard");
        rein.setSize(1000);
        rein.execute().actionGet().getHits().getHits();
        for(SearchHit hit :rein.execute().actionGet().getHits().getHits()){
            ZkilloardReady kill = mapper.readValue(hit.getSourceAsString(), ZkilloardReady.class);
            kill.setiVersion(0);
            esc.updateField("zkillboard",kill.getKillID()+"","iVersion",kill.getiVersion());
        }
        return Response.ok("done", MediaType.TEXT_PLAIN_TYPE).build();
    }



    @Path("newKM/{id}/{from}")
    @GET
    public Response newKM(@PathParam("from")int from,@PathParam("id")long id) throws IOException, InterruptedException {
        Deque<Long> allys = new ArrayDeque<>();

        allys.push(1354830081l);
        allys.push(1301367357l);
        allys.push(1147488332l);
        allys.push(99002938l);
        allys.push(131511956l);
        allys.push(1220174199l);
        allys.push(1988009451l);
        allys.push(1411711376l);
        allys.push(1463354890l);
        allys.push(1727758877l);
        allys.push(741557221l);
        allys.push(99003500l);
        allys.push(99001861l);
        allys.push(679584932l);



        while (allys.size()>0) {

            long aid = allys.pop();


            int i = 0;
            CountRequestBuilder count = esc.getClient().prepareCount(esc.getINDEX());
            count.setTypes("kills");
            count.setQuery(QueryBuilders.matchQuery("victim.allianceID", aid));
            CountResponse countResponse = count.execute().actionGet();

            while (i < countResponse.getCount()) {



                SearchRequestBuilder rein = esc.getClient().prepareSearch(esc.getINDEX());
                rein.setTypes("kills");
                rein.setQuery(QueryBuilders.matchQuery("victim.allianceID", aid));
                rein.addSort("killTime", SortOrder.ASC);
                rein.setFrom(i);
                rein.setSize(1000);

                SearchResponse r = rein.execute().actionGet();

                for (SearchHit hit : r.getHits().getHits()) {

                    if(hit.getSourceAsString()!=null) {
                        ZkilloardReady kill = mapper.readValue(hit.getSourceAsString(), ZkilloardReady.class);

//                        Killmail km = factory.fromZkillboard(kill);

//                        esc.update(mapper.writeValueAsString(km), "killmail", km.getKillID() + "");
                    }

                }
                System.out.print(i + " from " + countResponse.getCount());
                i = i + 1000;

                Thread.sleep(5000);
            }
            Thread.sleep(15000);
        }

        return Response.ok("done", MediaType.TEXT_PLAIN_TYPE).build();
    }


}
