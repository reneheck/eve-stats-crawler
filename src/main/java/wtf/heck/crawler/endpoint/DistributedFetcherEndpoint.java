package wtf.heck.crawler.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wtf.heck.elasticsearch.ElasticSearchClient;

import wtf.heck.eve.zkillboard.QueryStatus;
import wtf.heck.eve.zkillboard.ZkillboardQueryEntity;
import wtf.heck.eve.zkillboard.ZkillboardService;
import wtf.heck.eve.zkillboard.json.Zkilloard;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Date;

/**
 * Created by cerias on 04.04.15.
 */

@Path("dist")
public class DistributedFetcherEndpoint {
    Logger log = LoggerFactory.getLogger(DistributedFetcherEndpoint.class);
    ObjectMapper mapper = new ObjectMapper();

    @Inject
    ZkillboardService zService;

    @Inject
    ElasticSearchClient esc;

    private final String TYPE = "requests";

    @Path("get/{server}")
    @GET
    public Response getQuery(@PathParam("server") String server) throws IOException {
//        ZkillboardQueryEntity queryEntity;
//
//
//        BoolQueryBuilder queryBuilder = new BoolQueryBuilder();
//        queryBuilder.must(QueryBuilders.matchQuery("queryStatus", "WAITING"));
//        queryBuilder.must(QueryBuilders.rangeQuery("lastUpdate").lt((new Date().getTime() - (1000 * 60 * 1))));
//
//        SearchRequestBuilder searchRequest = esc.getClient().prepareSearch(esc.getINDEX());
//        searchRequest.setTypes(TYPE);
//        searchRequest.setSize(10);
//        searchRequest.addSort("lastUpdate", SortOrder.ASC);
//        searchRequest.setQuery(queryBuilder);
//
//        SearchResponse response = searchRequest.execute().actionGet();
//
//        if (response.getHits().getHits().length>0){
//            Random generator = new Random();
//            int i = generator.nextInt(response.getHits().getHits().length) ;
//            queryEntity = mapper.readValue(response.getHits().getAt(i).getSourceAsString(), ZkillboardQueryEntity.class);
//
//            if (queryEntity.getEsId() == null) {
//                queryEntity.setEsId(response.getHits().getAt(i).getId());
//                log.info("Setting ES ID: " + response.getHits().getAt(i).getId());
//            }
//                String queryString = queryEntity.getType() + "/" + queryEntity.getId() + "/losses/";
//                if (queryEntity.getQueryList().contains("forward")) {
//                    queryString = queryString + "afterKillID/" + queryEntity.getKillID() + "/orderDirection/asc/";
//                }
//
//                queryEntity.setQueryStatus(QueryStatus.INQUERY);
//                queryEntity.setEsc(esc);
//                queryEntity.setServer(server);
//                queryEntity.update();
//                log.warn("Sending "+ queryString.toString()+ " to: "+server);
//                Map<String, String> returnMap = new HashMap<>();
//                returnMap.put("url", "https://zkillboard.com/api/" + queryString);
//                returnMap.put("id", queryEntity.getEsId());
//
//                return Response.ok(mapper.writeValueAsString(returnMap)).build();
//            } else {
//                return Response.ok("{}").build();
//            }

        return Response.ok("{}").build();
    }


    @Path("insert/{server}/{id}")
    @POST
    public Response putKills(String body, @PathParam("server") String server, @PathParam("id") String id)  {

        log.info("server: "+server+" id:"+id);
        ZkillboardQueryEntity queryEntity = null;
        try {
            queryEntity = (ZkillboardQueryEntity) esc.get("requests", id, ZkillboardQueryEntity.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            zService.insert(queryEntity, mapper.readValue(body, Zkilloard[].class));
        } catch (IOException e) {
            e.printStackTrace();
        }


        return Response.ok("ack").build();
    }

    @Path("repair")
    @GET
    public Response repair() throws IOException {
        log.info("repairing querys");
        BoolQueryBuilder queryBuilder = new BoolQueryBuilder();
        queryBuilder.must(QueryBuilders.matchQuery("queryStatus", "INQUERY"));
        queryBuilder.must(QueryBuilders.rangeQuery("lastUpdate").lt((new Date().getTime() - (1000 * 60 * 15))));

        SearchRequestBuilder searchRequest = esc.getClient().prepareSearch(esc.getINDEX());
        searchRequest.setTypes(TYPE);
        searchRequest.setSize(100);
        searchRequest.setQuery(queryBuilder);

        SearchResponse response = searchRequest.execute().actionGet();

        for (SearchHit hit : response.getHits().getHits()) {
            ZkillboardQueryEntity zqe = mapper.readValue(hit.getSourceAsString(), ZkillboardQueryEntity.class);
            zqe.setQueryStatus(QueryStatus.WAITING);
            log.warn("Reparing Query: " + zqe.getName());
            zqe.setEsc(esc);

            zqe.update();
        }

    return Response.ok("repaird").build();
    }
}
