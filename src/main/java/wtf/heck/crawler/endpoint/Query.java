package wtf.heck.crawler.endpoint;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.action.delete.DeleteRequestBuilder;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequestBuilder;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wtf.heck.elasticsearch.ElasticSearchClient;

import wtf.heck.eve.zkillboard.QueryStatus;
import wtf.heck.eve.zkillboard.ZkillboardQueryEntity;
import wtf.heck.httpClients.RestClient;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.*;

/**
 * Created by cerias on 14.01.15.
 */
@Path("query")
public class Query {

    Logger log = LoggerFactory.getLogger(Query.class);

    private final String TYPE = "requests";
    @Inject
    ElasticSearchClient esc;


    ObjectMapper mapper = new ObjectMapper();

    @Path("add/alliance/{id}")
    @GET
    public Response add(@PathParam("id") String id) throws JsonProcessingException, InterruptedException {
        log.info("Added query");
        String resp = "whops";
        ZkillboardQueryEntity query = new ZkillboardQueryEntity();
        query.setQueryStatus(QueryStatus.WAITING);
        query.setType("allianceID");
        query.setId(Long.parseLong(id));
        query.setKillID(0);
        query.setLastUpdate(new Date());
        query.setQueryList("forward");
        SearchRequestBuilder requestBuilder = esc.getClient().prepareSearch(esc.getINDEX());
        requestBuilder.setTypes(TYPE);
        requestBuilder.setQuery(QueryBuilders.boolQuery().must(QueryBuilders.matchQuery("id", id)).must(QueryBuilders.matchQuery("type", "allianceID")));
        if (requestBuilder.execute().actionGet().getHits().getTotalHits() == 0) {
            esc.insert(mapper.writeValueAsString(query), TYPE);
            resp = "ID: " + id + " added";
        } else {
            resp = "Already exists";
        }

        return Response.ok(resp, MediaType.TEXT_PLAIN_TYPE).build();
    }

    @Path("add/corp/{id}")
    @GET
    public Response addCorp(@PathParam("id") String id) throws JsonProcessingException, InterruptedException {
        log.info("Added query");
        String resp = "whops";
        ZkillboardQueryEntity query = new ZkillboardQueryEntity();
        query.setQueryStatus(QueryStatus.WAITING);
        query.setType("corporationID");
        query.setId(Long.parseLong(id));
        query.setKillID(0);
        query.setLastUpdate(new Date());
        query.setQueryList("forward");
        SearchRequestBuilder requestBuilder = esc.getClient().prepareSearch(esc.getINDEX());
        requestBuilder.setTypes(TYPE);
        requestBuilder.setQuery(QueryBuilders.boolQuery().must(QueryBuilders.matchQuery("id", id)).must(QueryBuilders.matchQuery("type", "corporationID")));
        if (requestBuilder.execute().actionGet().getHits().getTotalHits() == 0) {
            esc.insert(mapper.writeValueAsString(query), TYPE);
            resp = "ID: " + id + " added";
        } else {
            resp = "Already exists";
        }

        return Response.ok(resp, MediaType.TEXT_PLAIN_TYPE).build();
    }

    @Path("add/group/{id}/{list}/{start}")
    @GET
    public Response addGroup(@PathParam("id") String id,@PathParam("list")String list,@PathParam("start")String start) throws JsonProcessingException, InterruptedException {
        log.info("Added query");
        String resp = "whops";
        ZkillboardQueryEntity query = new ZkillboardQueryEntity();
        query.setQueryStatus(QueryStatus.WAITING);
        query.setType("groupID");
        query.setId(Long.parseLong(id));
        query.setKillID(Long.parseLong(start));
        query.setLastUpdate(new Date());
        query.setQueryList(list);
//        SearchRequestBuilder requestBuilder = esc.getClient().prepareSearch(esc.getINDEX());
//        requestBuilder.setTypes(TYPE);
//        requestBuilder.setQuery(QueryBuilders.boolQuery().must(QueryBuilders.matchQuery("id", id)).must(QueryBuilders.matchQuery("type", "groupID")));
//        if (requestBuilder.execute().actionGet().getHits().getTotalHits() == 0) {
            esc.insert(mapper.writeValueAsString(query), TYPE);
            resp = "ID: " + id + " added";
//        } else {
//            resp = "Already exists";
//        }

        return Response.ok(resp, MediaType.TEXT_PLAIN_TYPE).build();
    }


    @Path("inquery")
    @GET
    public Response inquery() throws IOException {

        ArrayList<ZkillboardQueryEntity> redisList = new ArrayList<>();



        BoolQueryBuilder queryBuilder = new BoolQueryBuilder();
        queryBuilder.must(QueryBuilders.matchAllQuery());


        SearchRequestBuilder searchRequest = esc.getClient().prepareSearch(esc.getINDEX());
        searchRequest.setTypes(TYPE);
        searchRequest.setSize(1000);
        searchRequest.setQuery(QueryBuilders.matchAllQuery());


        SearchResponse response = searchRequest.execute().actionGet();
        ArrayList<Map<String, Object>> list = new ArrayList<>();
        for (SearchHit hit : response.getHits().getHits()) {

            Map<String, Object> m = new HashMap<>();
            ZkillboardQueryEntity zqe = mapper.readValue(hit.getSourceAsString(), ZkillboardQueryEntity.class);

            m.put("name", zqe.getName());
            m.put("id", zqe.getId());
            m.put("esId", zqe.getEsId());
            m.put("lastUpdate", zqe.getLastUpdate());
            m.put("list", zqe.getQueryList());
            m.put("type", zqe.getType());
            m.put("killID", zqe.getKillID());
            m.put("killTime",zqe.getKillTime());
            m.put("status", zqe.getQueryStatus());

            list.add(m);
        }
        return Response.ok(mapper.writeValueAsString(list), MediaType.TEXT_PLAIN_TYPE).build();
    }

    @Path("convert")
    @GET
    public Response convert() throws IOException {
        String lastId = "46091007";
        BoolQueryBuilder queryBuilder = new BoolQueryBuilder();
//        queryBuilder.must(QueryBuilders.termQuery("queryList", "forward"));
//        queryBuilder.must(QueryBuilders.termQuery("type","groupID"));
        queryBuilder.must(QueryBuilders.matchAllQuery());

        SearchRequestBuilder searchRequest = esc.getClient().prepareSearch(esc.getINDEX());
        searchRequest.setTypes(TYPE);
        searchRequest.setSize(1000);
        searchRequest.setQuery(queryBuilder);


        for(SearchHit hit:searchRequest.execute().actionGet().getHits().getHits()){
            ZkillboardQueryEntity zqe = mapper.readValue(hit.getSourceAsString(), ZkillboardQueryEntity.class);
            log.info(zqe.toString());
            zqe.setQueryList("backward");
            zqe.setKillID(46091007);
            zqe.setEsId(null);
            esc.insert(mapper.writeValueAsString(zqe),TYPE);

        }

        return Response.ok("ack", MediaType.TEXT_PLAIN_TYPE).build();
    }

    @Path("activate/{id}")
    @GET
    public Response activate(@PathParam("id") String id) throws IOException {
        GetRequestBuilder get = esc.getClient().prepareGet();
        get.setId(id);
        get.setIndex(esc.getINDEX());
        get.setType("requests");

        GetResponse gresp = get.execute().actionGet();

        if (gresp.isExists()) {
            ZkillboardQueryEntity zqe = mapper.readValue(gresp.getSourceAsString(), ZkillboardQueryEntity.class);
            zqe.setEsc(esc);
            zqe.setEsId(gresp.getId());
            zqe.setQueryStatus(QueryStatus.WAITING);
            zqe.setKillTime(new Date());
            zqe.update();
            zqe.addToQuery();
            System.out.println(zqe.toString());
            return Response.ok(mapper.writeValueAsString(zqe), MediaType.APPLICATION_JSON_TYPE).build();
        } else {
            return Response.serverError().build();
        }
    }


    @Path("delete/{id}")
    @GET
    public Response delete(@PathParam("id") String id) throws IOException {
        DeleteRequestBuilder get = esc.getClient().prepareDelete();
        get.setId(id);
        get.setIndex(esc.getINDEX());
        get.setType("requests");

        DeleteResponse gresp = get.execute().actionGet();

        if (gresp.isFound()) {

            return Response.ok("{\"name\":\"DELETED\"}", MediaType.APPLICATION_JSON_TYPE).build();
        } else {
            return Response.serverError().build();
        }

    }

    @Path("test")
    @GET
    public Response test() throws IOException {
        RestClient restClient = new RestClient("https://zkillboard.com/api/losses/groupID/26/afterKillID/1/");


        return Response.ok(restClient.getResponse()).build();
    }
    @Path("fillQuery")
    @GET
    public Response fillQuery() throws IOException {


        BoolQueryBuilder queryBuilder = new BoolQueryBuilder();
        queryBuilder.mustNot(QueryBuilders.matchQuery("queryStatus", "DONE"));
        queryBuilder.mustNot(QueryBuilders.matchQuery("queryStatus", "ERROR"));
//        queryBuilder.must(QueryBuilders.matchQuery("queryList", "backward"));
//        queryBuilder.must(QueryBuilders.matchQuery("type","groupID"));
        queryBuilder.must(QueryBuilders.rangeQuery("lastUpdate").lt((new Date().getTime() - ((long) 1000 * 60 * 1))));

        SearchRequestBuilder searchRequest = esc.getClient().prepareSearch(esc.getINDEX());
        searchRequest.setTypes(TYPE);
        searchRequest.setSize(100);
        searchRequest.setQuery(queryBuilder);


        SearchResponse response = searchRequest.execute().actionGet();
        log.warn("Adding to Query " + response.getHits().getTotalHits() + "...");
        for (SearchHit hit : response.getHits().getHits()) {
            ZkillboardQueryEntity zqe = mapper.readValue(hit.getSourceAsString(), ZkillboardQueryEntity.class);

            zqe.setEsc(esc);

            zqe.setEsId(hit.getId());
            zqe.update();
            zqe.addToQuery();

        }

        return Response.ok("ok", MediaType.TEXT_PLAIN_TYPE).build();
    }



}
