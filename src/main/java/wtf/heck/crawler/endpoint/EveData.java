package wtf.heck.crawler.endpoint;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import wtf.heck.elasticsearch.ElasticSearchClient;
import wtf.heck.eve.crest.ItemGenerator;
import wtf.heck.eve.crest.SolarGenerator;
import wtf.heck.eve.crest.groups.Groups;
import wtf.heck.eve.crest.market.groups.MarketGroups;
import wtf.heck.httpClients.CrestClient;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.io.IOException;

/**
 * Created by cerias on 24.04.15.
 */
@Path("eve/data/import")
public class EveData {

    @Inject
    ElasticSearchClient esc;

    @Path("/items")
    @GET
    public Response items() throws IOException, InterruptedException {
        ObjectMapper mapper = new ObjectMapper();
        CrestClient marketGroups = new CrestClient("http://public-crest.eveonline.com/market/groups/");
        CrestClient invGroups = new CrestClient("http://public-crest.eveonline.com/inventory/groups/");

        MarketGroups mg = mapper.readValue(marketGroups.getResponse(), MarketGroups.class);
        Groups groups = mapper.readValue(invGroups.getResponse(), Groups.class);
        ItemGenerator itemGenerator = new ItemGenerator(mg,groups);
//        itemGenerator.loadProduction();
//        itemGenerator.calcMinerals();
        itemGenerator.load();
        itemGenerator.makeItems();

        return Response.ok().build();
    }

    @Path("solarSystem")
    @GET
    public Response solarSystem() throws IOException, InterruptedException {
        System.out.println("loading solarSystem");
        SolarGenerator generator = new SolarGenerator();
        generator.load();

        return Response.ok().build();
    }
}
