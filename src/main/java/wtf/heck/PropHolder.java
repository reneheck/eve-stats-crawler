package wtf.heck;

import javax.enterprise.context.ApplicationScoped;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by cerias on 06.05.15.
 */
@ApplicationScoped
public class PropHolder {

    Properties properties;

    public PropHolder(){
        InputStream inputStream  = PropHolder.class.getClassLoader().getResourceAsStream("basic.properties");
        this.properties = new Properties();
        try {
            this.properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getProperty(String key){
        return properties.getProperty(key);
    }
}
