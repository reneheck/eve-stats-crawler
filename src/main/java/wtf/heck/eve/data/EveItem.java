package wtf.heck.eve.data;



import java.util.ArrayList;

/**
 * Created by cerias on 22.02.15.
 */
public class EveItem {

    long id;
    String name;
    ArrayList<String> marketGroups = new ArrayList<String>();
    String invGroup;
    String invCategory;
    private ArrayList<Mineral> minerals = new ArrayList<Mineral>();

    public EveItem() {
    }

    public EveItem(long id) {
        this.id=id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getMarketGroups() {
        return marketGroups;
    }

    public void setMarketGroups(ArrayList<String> marketGroups) {
        this.marketGroups = marketGroups;
    }

    public void addMarketGroup(String marketGroup){
        this.marketGroups.add(marketGroup);
    }

    public ArrayList<Mineral> getMinerals() {
        return minerals;
    }

    public void setMinerals(ArrayList<Mineral> minerals) {
        this.minerals = minerals;
    }

    public String getInvGroup() {
        return invGroup;
    }

    public void setInvGroup(String invGroup) {
        this.invGroup = invGroup;
    }

    public String getInvCategory() {
        return invCategory;
    }

    public void setInvCategory(String invCategory) {
        this.invCategory = invCategory;
    }
}
