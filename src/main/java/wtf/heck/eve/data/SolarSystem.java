package wtf.heck.eve.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import wtf.heck.eve.crest.map.Position;

/**
 * Created by cerias on 24.04.15.
 */
public class SolarSystem {

    @JsonProperty("name")
    String name;
    @JsonProperty("position")
    Position position;
    @JsonProperty("securityStatus")
    private Double securityStatus;
    @JsonProperty("constellation")
    String constellation;
    @JsonProperty("region")
    String region;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Double getSecurityStatus() {
        return securityStatus;
    }

    public void setSecurityStatus(Double securityStatus) {
        this.securityStatus = securityStatus;
    }

    public String getConstellation() {
        return constellation;
    }

    public void setConstellation(String constellation) {
        this.constellation = constellation;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    @Override
    public String toString() {
        return "SolarSystem{" +
                "name='" + name + '\'' +
                ", position=" + position +
                ", securityStatus=" + securityStatus +
                ", constellation='" + constellation + '\'' +
                ", region='" + region + '\'' +
                '}';
    }
}
