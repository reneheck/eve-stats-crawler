package wtf.heck.eve.data;

/**
 * Created by cerias on 20.02.15.
 */
public class Mineral {

    long id;
    long quantity;



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public Mineral() {

    }

    public Mineral(long id, long quantity) {
        this.id = id;
        this.quantity = quantity;
    }
}
