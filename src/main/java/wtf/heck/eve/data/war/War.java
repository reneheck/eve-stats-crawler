package wtf.heck.eve.data.war;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

/**
 * Created by cerias on 06.05.15.
 */
public class War {

    @JsonProperty("id")
    long id;
    @JsonProperty("mutual")
    boolean mutual;
    @JsonProperty("lastUpdate")
    Date lastUpdate;
    @JsonProperty("start")
    Date start;
    @JsonProperty("end")
    Date end;
    @JsonProperty("aggressor")
    WarEntity aggressor;
    @JsonProperty("defender")
    WarEntity defender;
    @JsonProperty("openForAllies")
    boolean openForAllies;

    public War(){

    }

    public War(long id){
        this.id=id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isMutual() {
        return mutual;
    }

    public void setMutual(boolean mutual) {
        this.mutual = mutual;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public WarEntity getAggressor() {
        return aggressor;
    }

    public void setAggressor(WarEntity aggressor) {
        this.aggressor = aggressor;
    }

    public WarEntity getDefender() {
        return defender;
    }

    public void setDefender(WarEntity defender) {
        this.defender = defender;
    }

    public boolean isOpenForAllies() {
        return openForAllies;
    }

    public void setOpenForAllies(boolean openForAllies) {
        this.openForAllies = openForAllies;
    }
}
