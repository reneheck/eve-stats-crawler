package wtf.heck.eve.data.war;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by cerias on 06.05.15.
 */
public class WarEntity {

    @JsonProperty("id")
    long id;
    @JsonProperty("name")
    String name;
    @JsonProperty("shipsKilled")
    long shipsKilled;
    @JsonProperty("iskKilled")
    double iskKilled;

    public WarEntity(){}

    public WarEntity(long id, String name, long shipsKilled, double iskKilled) {
        this.id = id;
        this.name = name;
        this.shipsKilled = shipsKilled;
        this.iskKilled = iskKilled;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getShipsKilled() {
        return shipsKilled;
    }

    public void setShipsKilled(long shipsKilled) {
        this.shipsKilled = shipsKilled;
    }

    public double getIskKilled() {
        return iskKilled;
    }

    public void setIskKilled(double iskKilled) {
        this.iskKilled = iskKilled;
    }
}
