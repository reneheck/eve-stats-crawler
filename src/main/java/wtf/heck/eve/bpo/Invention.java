
package wtf.heck.eve.bpo;

import com.fasterxml.jackson.annotation.*;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "materials",
    "products",
    "skills",
    "time"
})
public class Invention {

    @JsonProperty("materials")
    private List<Material> materials = new ArrayList<Material>();
    @JsonProperty("products")
    private List<Product> products = new ArrayList<Product>();
    @JsonProperty("skills")
    private List<Skill> skills = new ArrayList<Skill>();
    @JsonProperty("time")
    private Integer time;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The materials
     */
    @JsonProperty("materials")
    public List<Material> getMaterials() {
        return materials;
    }

    /**
     * 
     * @param materials
     *     The materials
     */
    @JsonProperty("materials")
    public void setMaterials(List<Material> materials) {
        this.materials = materials;
    }

    /**
     * 
     * @return
     *     The products
     */
    @JsonProperty("products")
    public List<Product> getProducts() {
        return products;
    }

    /**
     * 
     * @param products
     *     The products
     */
    @JsonProperty("products")
    public void setProducts(List<Product> products) {
        this.products = products;
    }

    /**
     * 
     * @return
     *     The skills
     */
    @JsonProperty("skills")
    public List<Skill> getSkills() {
        return skills;
    }

    /**
     * 
     * @param skills
     *     The skills
     */
    @JsonProperty("skills")
    public void setSkills(List<Skill> skills) {
        this.skills = skills;
    }

    /**
     * 
     * @return
     *     The time
     */
    @JsonProperty("time")
    public Integer getTime() {
        return time;
    }

    /**
     * 
     * @param time
     *     The time
     */
    @JsonProperty("time")
    public void setTime(Integer time) {
        this.time = time;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
