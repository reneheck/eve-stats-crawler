
package wtf.heck.eve.bpo;

import com.fasterxml.jackson.annotation.*;
import wtf.heck.eve.data.Mineral;


import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "activities",
    "blueprintTypeID",
    "maxProductionLimit"
})
public class Blueprint {

    @JsonProperty("activities")
    private Activities activities;
    @JsonProperty("blueprintTypeID")
    private Integer blueprintTypeID;
    @JsonProperty("maxProductionLimit")
    private Integer maxProductionLimit;

    private ArrayList<Mineral> minerals = new ArrayList<Mineral>();


    public ArrayList<Mineral> getMinerals() {
        return minerals;
    }

    public void setMinerals(ArrayList<Mineral> minerals) {
        this.minerals = minerals;
    }

    public void addMinerals(Mineral mineral){
        boolean contains = false;

        for(Mineral mins:this.minerals){
            if(mins.getId()== mineral.getId()){
                contains=true;
                mins.setQuantity(mins.getQuantity()+ mineral.getQuantity());
            }
        }
        if(!contains){
            minerals.add(mineral);
        }

    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The activities
     */
    @JsonProperty("activities")
    public Activities getActivities() {
        return activities;
    }

    /**
     * 
     * @param activities
     *     The activities
     */
    @JsonProperty("activities")
    public void setActivities(Activities activities) {
        this.activities = activities;
    }

    /**
     * 
     * @return
     *     The blueprintTypeID
     */
    @JsonProperty("blueprintTypeID")
    public Integer getBlueprintTypeID() {
        return blueprintTypeID;
    }

    /**
     * 
     * @param blueprintTypeID
     *     The blueprintTypeID
     */
    @JsonProperty("blueprintTypeID")
    public void setBlueprintTypeID(Integer blueprintTypeID) {
        this.blueprintTypeID = blueprintTypeID;
    }

    /**
     * 
     * @return
     *     The maxProductionLimit
     */
    @JsonProperty("maxProductionLimit")
    public Integer getMaxProductionLimit() {
        return maxProductionLimit;
    }

    /**
     * 
     * @param maxProductionLimit
     *     The maxProductionLimit
     */
    @JsonProperty("maxProductionLimit")
    public void setMaxProductionLimit(Integer maxProductionLimit) {
        this.maxProductionLimit = maxProductionLimit;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
