
package wtf.heck.eve.bpo;

import com.fasterxml.jackson.annotation.*;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "copying",
    "invention",
    "manufacturing",
    "research_material",
    "research_time"
})
public class Activities {

    @JsonProperty("copying")
    private Copying copying;
    @JsonProperty("invention")
    private Invention invention;
    @JsonProperty("manufacturing")
    private Manufacturing manufacturing;
    @JsonProperty("research_material")
    private ResearchMaterial researchMaterial;
    @JsonProperty("research_time")
    private ResearchTime researchTime;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The copying
     */
    @JsonProperty("copying")
    public Copying getCopying() {
        return copying;
    }

    /**
     * 
     * @param copying
     *     The copying
     */
    @JsonProperty("copying")
    public void setCopying(Copying copying) {
        this.copying = copying;
    }

    /**
     * 
     * @return
     *     The invention
     */
    @JsonProperty("invention")
    public Invention getInvention() {
        return invention;
    }

    /**
     * 
     * @param invention
     *     The invention
     */
    @JsonProperty("invention")
    public void setInvention(Invention invention) {
        this.invention = invention;
    }

    /**
     * 
     * @return
     *     The manufacturing
     */
    @JsonProperty("manufacturing")
    public Manufacturing getManufacturing() {
        return manufacturing;
    }

    /**
     * 
     * @param manufacturing
     *     The manufacturing
     */
    @JsonProperty("manufacturing")
    public void setManufacturing(Manufacturing manufacturing) {
        this.manufacturing = manufacturing;
    }

    /**
     * 
     * @return
     *     The researchMaterial
     */
    @JsonProperty("research_material")
    public ResearchMaterial getResearchMaterial() {
        return researchMaterial;
    }

    /**
     * 
     * @param researchMaterial
     *     The research_material
     */
    @JsonProperty("research_material")
    public void setResearchMaterial(ResearchMaterial researchMaterial) {
        this.researchMaterial = researchMaterial;
    }

    /**
     * 
     * @return
     *     The researchTime
     */
    @JsonProperty("research_time")
    public ResearchTime getResearchTime() {
        return researchTime;
    }

    /**
     * 
     * @param researchTime
     *     The research_time
     */
    @JsonProperty("research_time")
    public void setResearchTime(ResearchTime researchTime) {
        this.researchTime = researchTime;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
