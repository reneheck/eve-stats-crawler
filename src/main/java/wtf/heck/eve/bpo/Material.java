
package wtf.heck.eve.bpo;

import com.fasterxml.jackson.annotation.*;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "quantity",
    "typeID"
})
public class Material {

    @JsonProperty("quantity")
    private Integer quantity;
    @JsonProperty("typeID")
    private Integer typeID;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The quantity
     */
    @JsonProperty("quantity")
    public Integer getQuantity() {
        return quantity;
    }

    /**
     * 
     * @param quantity
     *     The quantity
     */
    @JsonProperty("quantity")
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    /**
     * 
     * @return
     *     The typeID
     */
    @JsonProperty("typeID")
    public Integer getTypeID() {
        return typeID;
    }

    /**
     * 
     * @param typeID
     *     The typeID
     */
    @JsonProperty("typeID")
    public void setTypeID(Integer typeID) {
        this.typeID = typeID;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
