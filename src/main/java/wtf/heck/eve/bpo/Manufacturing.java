
package wtf.heck.eve.bpo;

import com.fasterxml.jackson.annotation.*;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "materials",
    "products",
    "skills",
    "time"
})
public class Manufacturing {

    @JsonProperty("materials")
    private List<Material_> materials = new ArrayList<Material_>();
    @JsonProperty("products")
    private List<Product_> products = new ArrayList<Product_>();
    @JsonProperty("skills")
    private List<Skill_> skills = new ArrayList<Skill_>();
    @JsonProperty("time")
    private Integer time;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The materials
     */
    @JsonProperty("materials")
    public List<Material_> getMaterials() {
        return materials;
    }

    /**
     * 
     * @param materials
     *     The materials
     */
    @JsonProperty("materials")
    public void setMaterials(List<Material_> materials) {
        this.materials = materials;
    }

    /**
     * 
     * @return
     *     The products
     */
    @JsonProperty("products")
    public List<Product_> getProducts() {
        return products;
    }

    /**
     * 
     * @param products
     *     The products
     */
    @JsonProperty("products")
    public void setProducts(List<Product_> products) {
        this.products = products;
    }

    /**
     * 
     * @return
     *     The skills
     */
    @JsonProperty("skills")
    public List<Skill_> getSkills() {
        return skills;
    }

    /**
     * 
     * @param skills
     *     The skills
     */
    @JsonProperty("skills")
    public void setSkills(List<Skill_> skills) {
        this.skills = skills;
    }

    /**
     * 
     * @return
     *     The time
     */
    @JsonProperty("time")
    public Integer getTime() {
        return time;
    }

    /**
     * 
     * @param time
     *     The time
     */
    @JsonProperty("time")
    public void setTime(Integer time) {
        this.time = time;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
