package wtf.heck.eve.crest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Files;
import lombok.extern.log4j.Log4j;
import org.apache.commons.io.Charsets;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.yaml.snakeyaml.Yaml;


import wtf.heck.PropHolder;
import wtf.heck.elasticsearch.ElasticSearchClient;
import wtf.heck.eve.bpo.Blueprint;
import wtf.heck.eve.crest.groups.Group;
import wtf.heck.eve.crest.groups.Groups;
import wtf.heck.eve.crest.groups.InvCategory;
import wtf.heck.eve.crest.groups.Type;
import wtf.heck.eve.crest.market.groups.Item;
import wtf.heck.eve.crest.market.groups.MarketGroups;
import wtf.heck.eve.crest.market.items.MarketGroupItems;

import wtf.heck.eve.bpo.Material_;
import wtf.heck.httpClients.CrestClient;
import wtf.heck.eve.data.EveItem;
import wtf.heck.eve.data.Mineral;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by cerias on 20.02.15.
 */
@RequestScoped

public class ItemGenerator {


//    @Inject
//    PropHolder prop;

    ElasticSearchClient esc = new ElasticSearchClient();
    Map<Long, Item> groups = new HashMap<Long, Item>();
    Map<Long, String> items = new HashMap<Long, String>();
    ArrayList<EveItem> itemReady = new ArrayList<EveItem>();
    ObjectMapper mapper = new ObjectMapper();
    MarketGroups mg;
    Groups groupRoot;
    Map<Long, String> itemGroup = new HashMap<>();
    Map<Long, wtf.heck.eve.crest.market.items.Item> itemsMarket = new HashMap<Long, wtf.heck.eve.crest.market.items.Item>();
    Map<Long, String> invCategory = new HashMap<>();
    Map<String, Long> itemGroupCategory = new HashMap<>();

    Map<Long, Blueprint> bpos = new HashMap<Long, Blueprint>();

    public ItemGenerator(MarketGroups mg, Groups groupsList) {
        this.mg = mg;
        this.groupRoot = groupsList;
    }

    public ItemGenerator() {
    }

    public void load() throws IOException, InterruptedException {
        for (Item item : mg.getItems()) {
            groups.put(getId(item.getHref()), item);


            CrestClient rc = new CrestClient(item.getTypes().getHref());

            MarketGroupItems mgi = mapper.readValue(rc.getResponse(), MarketGroupItems.class);

            for (wtf.heck.eve.crest.market.items.Item i : mgi.getItems()) {
                itemsMarket.put(Long.parseLong(i.getType().getIdStr()), i);
                items.put(Long.parseLong(i.getType().getIdStr()), i.getType().getName());
            }

            Thread.sleep(100);
            if (mgi.getNext() != null) {
                loadMarketPage(mgi.getNext().getHref());
            }

        }

        CrestClient cCat = new CrestClient("http://public-crest.eveonline.com/inventory/categories/");

        InvCategory invCategoryRoot = mapper.readValue(cCat.getResponse(), InvCategory.class);
        for (wtf.heck.eve.crest.Item i : invCategoryRoot.getItems()) {
            invCategory.put(getId(i.getHref()), i.getName());
        }
        for (wtf.heck.eve.crest.Item item : groupRoot.getItems()) {
            Thread.sleep(200);

            CrestClient rc = new CrestClient(item.getHref());
            Group group = mapper.readValue(rc.getResponse(), Group.class);
            if (!group.getAdditionalProperties().containsKey("exceptionType")) {
                itemGroupCategory.put(group.getName(), getId(group.getCategory().getHref()));
                for (Type t : group.getTypes()) {

                    itemGroup.put(getId(t.getHref()), group.getName());
                    if (!items.containsKey(getId(t.getHref()))) {
                        items.put(getId(t.getHref()), t.getName());
                    }

                }
            }

        }
    }

    private void loadMarketPage(String href) throws IOException, InterruptedException {
        CrestClient rc = new CrestClient(href);
        MarketGroupItems mgi = mapper.readValue(rc.getResponse(), MarketGroupItems.class);
        for (wtf.heck.eve.crest.market.items.Item i : mgi.getItems()) {
            itemsMarket.put(Long.parseLong(i.getType().getIdStr()), i);
        }
        Thread.sleep(500);
        if (mgi.getNext() != null) {
            loadMarketPage(mgi.getNext().getHref());
        }
    }

    public void loadProduction() throws IOException {
        Yaml yaml = new Yaml();
        String content = Files.toString(new File("/tmp/blueprints.yaml"), Charsets.UTF_8);
        Map<String, Object> result = (Map<String, Object>) yaml.load(content);

        for (Object name : result.keySet()) {
            wtf.heck.eve.bpo.Blueprint bpo = mapper.readValue(mapper.writeValueAsString(result.get(name)), Blueprint.class);

            if (bpo.getActivities() != null)
                if (bpo.getActivities().getManufacturing() != null)
                    if (bpo.getActivities().getManufacturing().getProducts() != null)
                        if (bpo.getActivities().getManufacturing().getProducts().size() == 1) {
                            bpos.put((long) bpo.getActivities().getManufacturing().getProducts().get(0).getTypeID(), bpo);
                        }

        }
    }

    public void calcMinerals() throws IOException {

        for (long bpoId : bpos.keySet()) {

            Blueprint bpo = bpos.get(bpoId);

            gatherMinerals(bpo);
            EveItem item = (EveItem) esc.get("itemTest", bpoId + "", EveItem.class);
            if (item != null) {
                item.setMinerals(bpo.getMinerals());
                esc.update(mapper.writeValueAsString(item), "item", bpoId + "");
            }
            System.out.println(mapper.writeValueAsString(bpo));


        }
    }

    public Blueprint gatherMinerals(Blueprint blueprint) {
        for (Material_ m : blueprint.getActivities().getManufacturing().getMaterials()) {
            Blueprint sbpo = bpos.get((long) m.getTypeID());
            if (sbpo == null) {
                System.out.println(m.getTypeID() + " | " + (long) Math.ceil((m.getQuantity() * 0.9)));
                blueprint.addMinerals(new Mineral(m.getTypeID(), (long) Math.round((m.getQuantity() * 0.9))));
            } else {

                gatherMinerals(sbpo, (long) Math.ceil((m.getQuantity() * 0.9)), blueprint);
            }
        }
        return blueprint;
    }

    public Blueprint gatherMinerals(Blueprint blueprint, long count, Blueprint toAdd) {
        for (Material_ m : blueprint.getActivities().getManufacturing().getMaterials()) {
            Blueprint sbpo = bpos.get((long) m.getTypeID());
            if (sbpo == null) {
                toAdd.addMinerals(new Mineral(m.getTypeID(), (long) Math.ceil(count * Math.ceil((m.getQuantity() * 0.9)))));
            } else {

                gatherMinerals(sbpo, (long) Math.ceil((m.getQuantity() * 0.9)), toAdd);
            }
        }
        return blueprint;
    }


    public void makeItems() throws JsonProcessingException, InterruptedException {

        System.out.println("start making items");
        BulkRequestBuilder bulkRequest = esc.getClient().prepareBulk();


        for (Map.Entry<Long, String> item : items.entrySet()) {
            EveItem newItem = new EveItem();
            newItem.setName(item.getValue());
            newItem.setId(item.getKey());

            newItem.setInvGroup(itemGroup.get(newItem.getId()));
            newItem.setInvCategory(invCategory.get(itemGroupCategory.get(newItem.getInvGroup())));

            if (itemsMarket.containsKey(newItem.getId())) {
                newItem.setMarketGroups(
                        getMarketGroups(
                                groups.get(
                                    (long) itemsMarket.get(newItem.getId()).getMarketGroup().getId()),
                                    new ArrayList<String>()
                        )
                );
            }

            bulkRequest.add(esc.getClient().prepareIndex(esc.getINDEX(), "items", newItem.getId()+"")
                    .setSource(mapper.writeValueAsString(newItem)));

        }

        System.out.println("inserting : "+bulkRequest.numberOfActions()+" items");
        if(bulkRequest.numberOfActions()>0) {
            BulkResponse responses = bulkRequest.execute().actionGet();

            if (responses.hasFailures()) {
                System.out.println(responses.buildFailureMessage());
            }
        }

    }

    public ArrayList<String> getMarketGroups(Item item, ArrayList<String> stringGroups) {
        stringGroups.add(item.getName());
        if (item.getParentGroup() != null) {
            return getMarketGroups(groups.get(getId(item.getParentGroup().getHref())), stringGroups);
        } else {
            return stringGroups;
        }
    }

    public long getId(String href) {
        String[] parts = href.split("/");
        return Long.parseLong(parts[parts.length - 1]);
    }

}
