package wtf.heck.eve.crest.map.constellation;

/**
 * Created by cerias on 24.04.15.
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import wtf.heck.eve.crest.Item;
import wtf.heck.eve.crest.map.Position;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "position",
        "region",
        "systems",
        "name"
})
public class Constellation {

    @JsonProperty("position")
    private Position position;
    @JsonProperty("region")
    private Item region;
    @JsonProperty("systems")
    private List<Item> systems = new ArrayList<Item>();
    @JsonProperty("name")
    private String name;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The position
     */
    @JsonProperty("position")
    public Position getPosition() {
        return position;
    }

    /**
     *
     * @param position
     * The position
     */
    @JsonProperty("position")
    public void setPosition(Position position) {
        this.position = position;
    }

    /**
     *
     * @return
     * The region
     */
    @JsonProperty("region")
    public Item getRegion() {
        return region;
    }

    /**
     *
     * @param region
     * The region
     */
    @JsonProperty("region")
    public void setRegion(Item region) {
        this.region = region;
    }

    /**
     *
     * @return
     * The systems
     */
    @JsonProperty("systems")
    public List<Item> getSystems() {
        return systems;
    }

    /**
     *
     * @param systems
     * The systems
     */
    @JsonProperty("systems")
    public void setSystems(List<Item> systems) {
        this.systems = systems;
    }

    /**
     *
     * @return
     * The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
