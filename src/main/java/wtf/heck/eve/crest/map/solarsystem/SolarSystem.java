package wtf.heck.eve.crest.map.solarsystem;

import com.fasterxml.jackson.annotation.JsonProperty;
import wtf.heck.eve.crest.Item;
import wtf.heck.eve.crest.map.Position;

/**
 * Created by cerias on 24.04.15.
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "stats",
        "name",
        "securityStatus",
        "securityClass",
        "href",
        "planets",
        "position",
        "sovereignty",
        "constellation"
})
public class SolarSystem {


    @JsonProperty("name")
    private String name;
    @JsonProperty("securityStatus")
    private Double securityStatus;
    @JsonProperty("securityClass")
    private String securityClass;
    @JsonProperty("href")
    private String href;
    @JsonProperty("planets")
    private List<Item> planets = new ArrayList<Item>();
    @JsonProperty("position")
    private Position position;
    @JsonProperty("sovereignty")
    private Sovereignty sovereignty;
    @JsonProperty("constellation")
    private Item constellation;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();



    /**
     *
     * @return
     * The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The securityStatus
     */
    @JsonProperty("securityStatus")
    public Double getSecurityStatus() {
        return securityStatus;
    }

    /**
     *
     * @param securityStatus
     * The securityStatus
     */
    @JsonProperty("securityStatus")
    public void setSecurityStatus(Double securityStatus) {
        this.securityStatus = securityStatus;
    }

    /**
     *
     * @return
     * The securityClass
     */
    @JsonProperty("securityClass")
    public String getSecurityClass() {
        return securityClass;
    }

    /**
     *
     * @param securityClass
     * The securityClass
     */
    @JsonProperty("securityClass")
    public void setSecurityClass(String securityClass) {
        this.securityClass = securityClass;
    }

    /**
     *
     * @return
     * The href
     */
    @JsonProperty("href")
    public String getHref() {
        return href;
    }

    /**
     *
     * @param href
     * The href
     */
    @JsonProperty("href")
    public void setHref(String href) {
        this.href = href;
    }

    /**
     *
     * @return
     * The planets
     */
    @JsonProperty("planets")
    public List<Item> getPlanets() {
        return planets;
    }

    /**
     *
     * @param planets
     * The planets
     */
    @JsonProperty("planets")
    public void setPlanets(List<Item> planets) {
        this.planets = planets;
    }

    /**
     *
     * @return
     * The position
     */
    @JsonProperty("position")
    public Position getPosition() {
        return position;
    }

    /**
     *
     * @param position
     * The position
     */
    @JsonProperty("position")
    public void setPosition(Position position) {
        this.position = position;
    }

    /**
     *
     * @return
     * The sovereignty
     */
    @JsonProperty("sovereignty")
    public Sovereignty getSovereignty() {
        return sovereignty;
    }

    /**
     *
     * @param sovereignty
     * The sovereignty
     */
    @JsonProperty("sovereignty")
    public void setSovereignty(Sovereignty sovereignty) {
        this.sovereignty = sovereignty;
    }

    /**
     *
     * @return
     * The constellation
     */
    @JsonProperty("constellation")
    public Item getConstellation() {
        return constellation;
    }

    /**
     *
     * @param constellation
     * The constellation
     */
    @JsonProperty("constellation")
    public void setConstellation(Item constellation) {
        this.constellation = constellation;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}