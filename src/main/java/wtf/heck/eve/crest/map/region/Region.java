package wtf.heck.eve.crest.map.region;

/**
 * Created by cerias on 24.04.15.
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import wtf.heck.eve.crest.Item;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "description",
        "marketBuyOrders",
        "name",
        "constellations",
        "marketSellOrders"
})
public class Region {

    @JsonProperty("description")
    private String description;

    @JsonProperty("name")
    private String name;
    @JsonProperty("constellations")
    private List<Item> constellations = new ArrayList<Item>();

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }



    /**
     *
     * @return
     * The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The constellations
     */
    @JsonProperty("constellations")
    public List<Item> getConstellations() {
        return constellations;
    }

    /**
     *
     * @param constellations
     * The constellations
     */
    @JsonProperty("constellations")
    public void setConstellations(List<Item> constellations) {
        this.constellations = constellations;
    }



    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
