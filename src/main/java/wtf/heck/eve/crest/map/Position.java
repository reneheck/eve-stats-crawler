package wtf.heck.eve.crest.map;

/**
 * Created by cerias on 24.04.15.
 */
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "y",
        "x",
        "z"
})
public class Position {

    @JsonProperty("y")
    private Double y;
    @JsonProperty("x")
    private Double x;
    @JsonProperty("z")
    private Double z;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The y
     */
    @JsonProperty("y")
    public Double getY() {
        return y;
    }

    /**
     *
     * @param y
     * The y
     */
    @JsonProperty("y")
    public void setY(Double y) {
        this.y = y;
    }

    /**
     *
     * @return
     * The x
     */
    @JsonProperty("x")
    public Double getX() {
        return x;
    }

    /**
     *
     * @param x
     * The x
     */
    @JsonProperty("x")
    public void setX(Double x) {
        this.x = x;
    }

    /**
     *
     * @return
     * The z
     */
    @JsonProperty("z")
    public Double getZ() {
        return z;
    }

    /**
     *
     * @param z
     * The z
     */
    @JsonProperty("z")
    public void setZ(Double z) {
        this.z = z;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}