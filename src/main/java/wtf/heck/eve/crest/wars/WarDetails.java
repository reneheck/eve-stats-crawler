
package wtf.heck.eve.crest.wars;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "timeFinished",
    "openForAllies",
    "timeStarted",
    "allyCount",
    "timeDeclared",
    "aggressor",
    "mutual",
    "allyCount_str",
    "killmails",
    "id_str",
    "defender",
    "id"
})
public class WarDetails {

    @JsonProperty("timeFinished")
    private Date timeFinished;
    @JsonProperty("openForAllies")
    private Boolean openForAllies;
    @JsonProperty("timeStarted")
    private Date timeStarted;
    @JsonProperty("allyCount")
    private Integer allyCount;
    @JsonProperty("timeDeclared")
    private Date timeDeclared;
    @JsonProperty("aggressor")
    private Aggressor aggressor;
    @JsonProperty("mutual")
    private Boolean mutual;
    @JsonProperty("allyCount_str")
    private String allyCountStr;
    @JsonProperty("killmails")
    private String killmails;
    @JsonProperty("id_str")
    private String idStr;
    @JsonProperty("defender")
    private Defender defender;
    @JsonProperty("id")
    private Integer id;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The timeFinished
     */
    @JsonProperty("timeFinished")
    public Date getTimeFinished() {
        return timeFinished;
    }

    /**
     * 
     * @param timeFinished
     *     The timeFinished
     */
    @JsonProperty("timeFinished")
    public void setTimeFinished(Date timeFinished) {
        this.timeFinished = timeFinished;
    }

    /**
     * 
     * @return
     *     The openForAllies
     */
    @JsonProperty("openForAllies")
    public Boolean getOpenForAllies() {
        return openForAllies;
    }

    /**
     * 
     * @param openForAllies
     *     The openForAllies
     */
    @JsonProperty("openForAllies")
    public void setOpenForAllies(Boolean openForAllies) {
        this.openForAllies = openForAllies;
    }

    /**
     * 
     * @return
     *     The timeStarted
     */
    @JsonProperty("timeStarted")
    public Date getTimeStarted() {
        return timeStarted;
    }

    /**
     * 
     * @param timeStarted
     *     The timeStarted
     */
    @JsonProperty("timeStarted")
    public void setTimeStarted(Date timeStarted) {
        this.timeStarted = timeStarted;
    }

    /**
     * 
     * @return
     *     The allyCount
     */
    @JsonProperty("allyCount")
    public Integer getAllyCount() {
        return allyCount;
    }

    /**
     * 
     * @param allyCount
     *     The allyCount
     */
    @JsonProperty("allyCount")
    public void setAllyCount(Integer allyCount) {
        this.allyCount = allyCount;
    }

    /**
     * 
     * @return
     *     The timeDeclared
     */
    @JsonProperty("timeDeclared")
    public Date getTimeDeclared() {
        return timeDeclared;
    }

    /**
     * 
     * @param timeDeclared
     *     The timeDeclared
     */
    @JsonProperty("timeDeclared")
    public void setTimeDeclared(Date timeDeclared) {
        this.timeDeclared = timeDeclared;
    }

    /**
     * 
     * @return
     *     The aggressor
     */
    @JsonProperty("aggressor")
    public Aggressor getAggressor() {
        return aggressor;
    }

    /**
     * 
     * @param aggressor
     *     The aggressor
     */
    @JsonProperty("aggressor")
    public void setAggressor(Aggressor aggressor) {
        this.aggressor = aggressor;
    }

    /**
     * 
     * @return
     *     The mutual
     */
    @JsonProperty("mutual")
    public Boolean getMutual() {
        return mutual;
    }

    /**
     * 
     * @param mutual
     *     The mutual
     */
    @JsonProperty("mutual")
    public void setMutual(Boolean mutual) {
        this.mutual = mutual;
    }

    /**
     * 
     * @return
     *     The allyCountStr
     */
    @JsonProperty("allyCount_str")
    public String getAllyCountStr() {
        return allyCountStr;
    }

    /**
     * 
     * @param allyCountStr
     *     The allyCount_str
     */
    @JsonProperty("allyCount_str")
    public void setAllyCountStr(String allyCountStr) {
        this.allyCountStr = allyCountStr;
    }

    /**
     * 
     * @return
     *     The killmails
     */
    @JsonProperty("killmails")
    public String getKillmails() {
        return killmails;
    }

    /**
     * 
     * @param killmails
     *     The killmails
     */
    @JsonProperty("killmails")
    public void setKillmails(String killmails) {
        this.killmails = killmails;
    }

    /**
     * 
     * @return
     *     The idStr
     */
    @JsonProperty("id_str")
    public String getIdStr() {
        return idStr;
    }

    /**
     * 
     * @param idStr
     *     The id_str
     */
    @JsonProperty("id_str")
    public void setIdStr(String idStr) {
        this.idStr = idStr;
    }

    /**
     * 
     * @return
     *     The defender
     */
    @JsonProperty("defender")
    public Defender getDefender() {
        return defender;
    }

    /**
     * 
     * @param defender
     *     The defender
     */
    @JsonProperty("defender")
    public void setDefender(Defender defender) {
        this.defender = defender;
    }

    /**
     * 
     * @return
     *     The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
