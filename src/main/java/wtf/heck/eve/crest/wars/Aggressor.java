
package wtf.heck.eve.crest.wars;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "shipsKilled",
    "shipsKilled_str",
    "name",
    "href",
    "id_str",
    "icon",
    "id",
    "iskKilled"
})
public class Aggressor {

    @JsonProperty("shipsKilled")
    private Integer shipsKilled;
    @JsonProperty("shipsKilled_str")
    private String shipsKilledStr;
    @JsonProperty("name")
    private String name;
    @JsonProperty("href")
    private String href;
    @JsonProperty("id_str")
    private String idStr;
    @JsonProperty("icon")
    private Icon icon;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("iskKilled")
    private Double iskKilled;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The shipsKilled
     */
    @JsonProperty("shipsKilled")
    public Integer getShipsKilled() {
        return shipsKilled;
    }

    /**
     * 
     * @param shipsKilled
     *     The shipsKilled
     */
    @JsonProperty("shipsKilled")
    public void setShipsKilled(Integer shipsKilled) {
        this.shipsKilled = shipsKilled;
    }

    /**
     * 
     * @return
     *     The shipsKilledStr
     */
    @JsonProperty("shipsKilled_str")
    public String getShipsKilledStr() {
        return shipsKilledStr;
    }

    /**
     * 
     * @param shipsKilledStr
     *     The shipsKilled_str
     */
    @JsonProperty("shipsKilled_str")
    public void setShipsKilledStr(String shipsKilledStr) {
        this.shipsKilledStr = shipsKilledStr;
    }

    /**
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The href
     */
    @JsonProperty("href")
    public String getHref() {
        return href;
    }

    /**
     * 
     * @param href
     *     The href
     */
    @JsonProperty("href")
    public void setHref(String href) {
        this.href = href;
    }

    /**
     * 
     * @return
     *     The idStr
     */
    @JsonProperty("id_str")
    public String getIdStr() {
        return idStr;
    }

    /**
     * 
     * @param idStr
     *     The id_str
     */
    @JsonProperty("id_str")
    public void setIdStr(String idStr) {
        this.idStr = idStr;
    }

    /**
     * 
     * @return
     *     The icon
     */
    @JsonProperty("icon")
    public Icon getIcon() {
        return icon;
    }

    /**
     * 
     * @param icon
     *     The icon
     */
    @JsonProperty("icon")
    public void setIcon(Icon icon) {
        this.icon = icon;
    }

    /**
     * 
     * @return
     *     The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The iskKilled
     */
    @JsonProperty("iskKilled")
    public Double getIskKilled() {
        return iskKilled;
    }

    /**
     * 
     * @param iskKilled
     *     The iskKilled
     */
    @JsonProperty("iskKilled")
    public void setIskKilled(Double iskKilled) {
        this.iskKilled = iskKilled;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
