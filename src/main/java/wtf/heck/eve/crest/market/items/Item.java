
package wtf.heck.eve.crest.market.items;

import com.fasterxml.jackson.annotation.*;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "marketGroup",
    "type"
})
public class Item {

    @JsonProperty("marketGroup")
    private MarketGroup marketGroup;
    @JsonProperty("type")
    private Type type;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The marketGroup
     */
    @JsonProperty("marketGroup")
    public MarketGroup getMarketGroup() {
        return marketGroup;
    }

    /**
     * 
     * @param marketGroup
     *     The marketGroup
     */
    @JsonProperty("marketGroup")
    public void setMarketGroup(MarketGroup marketGroup) {
        this.marketGroup = marketGroup;
    }

    /**
     * 
     * @return
     *     The type
     */
    @JsonProperty("type")
    public Type getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    @JsonProperty("type")
    public void setType(Type type) {
        this.type = type;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
