package wtf.heck.eve.crest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.OrFilterBuilder;
import org.elasticsearch.search.SearchHit;
import wtf.heck.elasticsearch.ElasticSearchClient;
import wtf.heck.eve.crest.wars.*;
import wtf.heck.eve.data.war.War;
import wtf.heck.eve.data.war.WarEntity;
import wtf.heck.httpClients.CrestClient;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by cerias on 06.05.15.
 */
@Stateless
public class WarWorker {

    ObjectMapper mapper = new ObjectMapper();

    @Inject
    ElasticSearchClient esc;

//    @Timeout
//    @AccessTimeout(value = 5, unit = TimeUnit.MINUTES)
//    @Schedule(hour = "*")
    public void crawlWars() throws IOException, InterruptedException {
        CrestClient crestClient = new CrestClient("http://public-crest.eveonline.com/wars/");

        WarPage page = mapper.readValue(crestClient.getResponse(), WarPage.class);

        newWars(page);

    }


//    @Timeout
//    @AccessTimeout(value = 5, unit = TimeUnit.MINUTES)
//    @Schedule(minute = "*",hour = "*")
    public void crawlWar() throws IOException, InterruptedException {

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR,-3);
        SearchRequestBuilder esRequest = esc.getClient().prepareSearch(esc.getINDEX());
        esRequest.setTypes("war");
        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        andFilterBuilder.add(FilterBuilders.notFilter(FilterBuilders.existsFilter("end")));

        OrFilterBuilder orFilter = new OrFilterBuilder();
        orFilter.add(FilterBuilders.rangeFilter("lastUpdate").lt(cal.getTime()));
        orFilter.add(FilterBuilders.notFilter(FilterBuilders.existsFilter("lastUpdate")));
        andFilterBuilder.add(orFilter);
        esRequest.setPostFilter(andFilterBuilder);

        esRequest.setSize(10);


        SearchResponse response = esRequest.execute().actionGet();

        for(SearchHit hit:response.getHits().getHits()){
            War war = mapper.readValue(hit.getSourceAsString(),War.class);
            CrestClient crestClient = new CrestClient("http://public-crest.eveonline.com/wars/"+war.getId()+"/");
            WarDetails warDetails = mapper.readValue(crestClient.getResponse(), WarDetails.class);

            war.setStart(warDetails.getTimeStarted());
            if(warDetails.getTimeFinished()!=null)
                war.setEnd(warDetails.getTimeFinished());

            war.setAggressor(new WarEntity(warDetails.getAggressor().getId(),warDetails.getAggressor().getName(),warDetails.getAggressor().getShipsKilled(),warDetails.getAggressor().getIskKilled()));
            war.setDefender(new WarEntity(warDetails.getDefender().getId(), warDetails.getDefender().getName(), warDetails.getDefender().getShipsKilled(), warDetails.getDefender().getIskKilled()));
            war.setMutual(warDetails.getMutual());
            war.setOpenForAllies(warDetails.getOpenForAllies());
            war.setLastUpdate(new Date());


            esc.insert(mapper.writeValueAsString(war),"war",war.getId()+"");
        }



    }






    private void newWars(WarPage page) throws IOException, InterruptedException {
        //insert new wars
        for(wtf.heck.eve.crest.wars.Item item :page.getItems()){
            War war = (War)esc.get("war", item.getIdStr(), War.class);
            if(war==null){
                war = new War((long)item.getId());
            }
        }

        //has next page
        if(page.getNext()!=null){
            Thread.sleep(1000);
            CrestClient crestClient = new CrestClient(page.getNext().getHref());
            newWars(mapper.readValue(crestClient.getResponse(),WarPage.class));
        }
    }
}
