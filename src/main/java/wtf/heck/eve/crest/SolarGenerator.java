package wtf.heck.eve.crest;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wtf.heck.elasticsearch.ElasticSearchClient;
import wtf.heck.eve.crest.map.constellation.Constellation;
import wtf.heck.eve.crest.map.region.Region;
import wtf.heck.eve.crest.map.region.Regions;
import wtf.heck.eve.crest.map.solarsystem.SolarSystem;
import wtf.heck.httpClients.CrestClient;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by cerias on 24.04.15.
 */

public class SolarGenerator {
    Logger log = LoggerFactory.getLogger(SolarGenerator.class);
    ObjectMapper mapper = new ObjectMapper();


    ElasticSearchClient esc = new ElasticSearchClient();

    public SolarGenerator(){

    }

    public void load() throws IOException, InterruptedException {
        CrestClient crestRegions = new CrestClient("http://public-crest.eveonline.com/regions/");

        Regions r = mapper.readValue(crestRegions.getResponse(), Regions.class);

        for(Item i:r.getItems()){
            Thread.sleep(500);
            log.info(i.getName());
            loadRegion(i.getHref(), i.getName());
        }


    }

    private void loadRegion(String href,String region) throws IOException, InterruptedException {
        CrestClient cRegion = new CrestClient(href);


        Region constellation = mapper.readValue(cRegion.getResponse(),Region.class);
        for(Item i:constellation.getConstellations()){
            Thread.sleep(100);

            loadConstellation(i.getHref(), region);
        }

    }
    private void loadConstellation(String href,String region) throws IOException, InterruptedException {
        CrestClient cConstellation = new CrestClient(href);


        Constellation constellation = mapper.readValue(cConstellation.getResponse(),Constellation.class);
        for(Item i:constellation.getSystems()){
            Thread.sleep(100);
            loadSolarSystem(i.getHref(), region, constellation.getName());
        }

    }

    private void loadSolarSystem(String href,String region,String constellation) throws IOException {
        CrestClient cSystem = new CrestClient(href);
        SolarSystem solarSystem = mapper.readValue(cSystem.getResponse(),SolarSystem.class);
        wtf.heck.eve.data.SolarSystem esSolarSystem = new wtf.heck.eve.data.SolarSystem();

        esSolarSystem.setName(solarSystem.getName());
        esSolarSystem.setConstellation(constellation);
        esSolarSystem.setRegion(region);
        esSolarSystem.setSecurityStatus(solarSystem.getSecurityStatus());
        esSolarSystem.setPosition(solarSystem.getPosition());

        esc.insert(mapper.writeValueAsString(esSolarSystem),"solarSystem",getId(solarSystem.getHref())+"");

    }

    public long getId(String href) {
        String[] parts = href.split("/");
        return Long.parseLong(parts[parts.length-1]);
    }
}
