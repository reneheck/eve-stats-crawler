
package wtf.heck.eve.zkillboard.json;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import javax.annotation.Generated;
import java.util.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "killID",
    "solarSystemID",
    "killTime",
    "moonID",
    "victim",
    "attackers",
    "items",
    "zkb"
})
public class Zkilloard {

    @JsonProperty("killID")
    private long killID;
    @JsonProperty("solarSystemID")
    private long solarSystemID;
    @JsonDeserialize(using = ZkillboardDateDeserilizer.class)
    @JsonProperty("killTime")
    private Date killTime;
    @JsonProperty("moonID")
    private String moonID;
    @JsonProperty("victim")
    private Victim victim;
    @JsonProperty("attackers")
    private List<Attacker> attackers = new ArrayList<Attacker>();
    @JsonProperty("items")
    private List<Item> items = new ArrayList<Item>();
    @JsonProperty("zkb")
    private Zkb zkb;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("killID")
    public long getKillID() {
        return killID;
    }

    @JsonProperty("killID")
    public void setKillID(long killID) {
        this.killID = killID;
    }

    @JsonProperty("solarSystemID")
    public long getSolarSystemID() {
        return solarSystemID;
    }

    @JsonProperty("solarSystemID")
    public void setSolarSystemID(long solarSystemID) {
        this.solarSystemID = solarSystemID;
    }

    @JsonProperty("killTime")
    public Date getKillTime() {
        return killTime;
    }

    @JsonProperty("killTime")
    public void setKillTime(Date killTime) {
        this.killTime = killTime;
    }

    @JsonProperty("moonID")
    public String getMoonID() {
        return moonID;
    }

    @JsonProperty("moonID")
    public void setMoonID(String moonID) {
        this.moonID = moonID;
    }

    @JsonProperty("victim")
    public Victim getVictim() {
        return victim;
    }

    @JsonProperty("victim")
    public void setVictim(Victim victim) {
        this.victim = victim;
    }

    @JsonProperty("attackers")
    public List<Attacker> getAttackers() {
        return attackers;
    }

    @JsonProperty("attackers")
    public void setAttackers(List<Attacker> attackers) {
        this.attackers = attackers;
    }

    @JsonProperty("items")
    public List<Item> getItems() {
        return items;
    }

    @JsonProperty("items")
    public void setItems(List<Item> items) {
        this.items = items;
    }

    @JsonProperty("zkb")
    public Zkb getZkb() {
        return zkb;
    }

    @JsonProperty("zkb")
    public void setZkb(Zkb zkb) {
        this.zkb = zkb;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
