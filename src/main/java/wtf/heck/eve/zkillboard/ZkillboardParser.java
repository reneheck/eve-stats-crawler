package wtf.heck.eve.zkillboard;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.action.bulk.BulkRequestBuilder;

import org.elasticsearch.action.bulk.BulkResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wtf.heck.elasticsearch.ElasticSearchClient;

import wtf.heck.httpClients.RestClient;
import wtf.heck.stats.killmail.KillmailFactory;
import wtf.heck.eve.zkillboard.json.Zkilloard;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import javax.inject.Inject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by cerias on 11.09.14.
 */

@Stateless
public class ZkillboardParser {

    Logger log = LoggerFactory.getLogger(ZkillboardParser.class);
    private String status;
    private ObjectMapper mapper = new ObjectMapper();


    @Inject
    ElasticSearchClient esc;

    @Inject
    KillmailFactory killmailFactory;

    @Inject
    ZkillBoardStatus zkillBoardStatus;

    public ZkillboardParser() {

    }


    @PostConstruct
    public void init() {


    }


    public void saveApiResponse(ArrayList<Zkilloard> kills) {
        BulkRequestBuilder bulkRequest = esc.getClient().prepareBulk();
        for (Zkilloard kill : kills) {
            try {
                bulkRequest.add(esc.getClient().prepareIndex(esc.getINDEX(), "zkillboard", kill.getKillID() + "").setSource(mapper.writeValueAsString(kill)));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        BulkResponse responses = bulkRequest.execute().actionGet();

        if (responses.hasFailures()) {
            log.error(responses.buildFailureMessage());
        }

    }


    public void killsBefore(ZkillboardQueryEntity queryEntity) throws JsonProcessingException {
        String queryString = queryEntity.getType() + "/" + queryEntity.getId() + "/losses/";
        if (queryEntity.getQueryList().contains("backward")) {
            queryString = queryString + "beforeKillID/" + queryEntity.getKillID() + "/";
        }

        queryEntity.setEsc(esc);

        ArrayList<Zkilloard> kills = new ArrayList<>();
        kills.addAll(this.getApi(queryString));

        if (kills.size() > 0) {
            queryEntity.setFailCounter(0);
            saveApiResponse(kills);
            queryEntity.setKillTime(kills.get(kills.size() - 1).getKillTime());
            queryEntity.setKillID(kills.get(kills.size() - 1).getKillID());
            queryEntity.generateName(kills.get(kills.size() - 1));


            Calendar cal = Calendar.getInstance();
            cal.set(2014, 1, 1);

            if (cal.after(queryEntity.getKillTime().getTime())) {
                log.info("Query finished: " + queryEntity.toString());
                queryEntity.setQueryStatus(QueryStatus.DONE);
            } else {
//                log.info("Query success should be waiting " + queryEntity.toString());
                queryEntity.setQueryStatus(QueryStatus.WAITING);
            }


        } else {
            queryEntity.setFailCounter(queryEntity.failCounter + 1);


            if (queryEntity.getFailCounter() > 10) {
                queryEntity.setQueryStatus(QueryStatus.ERROR);
                log.error(queryEntity + " too many errors");
            } else {
                queryEntity.setQueryStatus(QueryStatus.WAITING);
                Date d = new Date();
                d.setTime(d.getTime() + (long) 1000 * 60 * 60 * 1);
                queryEntity.setLastUpdate(d);
            }
        }

        queryEntity.update();
    }

    public void killsAfter(ZkillboardQueryEntity queryEntity) throws JsonProcessingException {

        String queryString = queryEntity.getType() + "/" + queryEntity.getId() + "/losses/";
        if (queryEntity.getQueryList().contains("forward")) {
            queryString = queryString + "afterKillID/" + queryEntity.getKillID() + "/orderDirection/asc/";
        }

        queryEntity.setEsc(esc);

        ArrayList<Zkilloard> kills = new ArrayList<>();
        kills.addAll(this.getApi(queryString));

        if (kills.size() > 0) {
            queryEntity.setFailCounter(0);
            saveApiResponse(kills);
            queryEntity.setKillTime(kills.get(kills.size() - 1).getKillTime());
            queryEntity.setKillID(kills.get(kills.size() - 1).getKillID());
            queryEntity.generateName(kills.get(kills.size() - 1));

            //    log.info("Query success should be waiting " + queryEntity.toString());
                queryEntity.setQueryStatus(QueryStatus.WAITING);

//            } else {
//                log.info("Query success should be done " + queryEntity.toString());
//                queryEntity.setQueryStatus(QueryStatus.DONE);
//            }

        } else {
            queryEntity.setFailCounter(queryEntity.failCounter + 1);


            if (queryEntity.getFailCounter() > 12) {
                queryEntity.setQueryStatus(QueryStatus.ERROR);
                log.error(queryEntity + " too many errors");
            } else {
                queryEntity.setQueryStatus(QueryStatus.WAITING);
                Date d = new Date();

                d.setTime(d.getTime() + (long) 1000 * 60 * 60 * (1 + (queryEntity.getFailCounter() * queryEntity.getFailCounter())));
                queryEntity.setLastUpdate(d);
            }
        }

        queryEntity.update();
    }

    public ArrayList<Zkilloard> getApi(String params) {
        Zkilloard[] reponse;
        ArrayList<Zkilloard> resp = new ArrayList<>();


        String responseString = "";
        RestClient rc = new RestClient("https://zkillboard.com/api/" + params,zkillBoardStatus);
        try {
            if (rc.getResponse() != null) {

                reponse = mapper.readValue(rc.getResponse(), Zkilloard[].class);
                resp.addAll(Arrays.asList(reponse));
            }

        } catch (IOException e) {
            System.out.println(rc.getResponse());
        }


        return resp;
    }

}
