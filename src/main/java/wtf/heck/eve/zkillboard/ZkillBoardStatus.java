package wtf.heck.eve.zkillboard;

import javax.enterprise.context.ApplicationScoped;

/**
 * Created by cerias on 06.05.15.
 */
@ApplicationScoped
public class ZkillBoardStatus {

    long request;
    long maxCount;

    public ZkillBoardStatus(){
        maxCount=0;
    }

    public long getRequest() {
        return request;
    }

    public void setRequest(long request) {
        this.request = request;
    }

    public long getMaxCount() {
        return maxCount;
    }

    public void setMaxCount(long maxCount) {
        this.maxCount = maxCount;
    }
}
