package wtf.heck.eve.zkillboard;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wtf.heck.elasticsearch.ElasticSearchClient;

import wtf.heck.eve.zkillboard.json.Zkilloard;

import java.util.Date;

/**
 * Created by cerias on 11.10.14.
 */
public class ZkillboardQueryEntity {
    Logger log = LoggerFactory.getLogger(ZkillboardQueryEntity.class);
    @JsonIgnore
    ElasticSearchClient esc;


    @JsonIgnore
    private static String TYPE = "requests";

    @JsonProperty
    String server;

    @JsonProperty
    long id;

    @JsonProperty
    String name;

    @JsonProperty
    long killID;
    @JsonProperty
    String type;

    @JsonProperty
    String queryList;

    @JsonProperty
    Date to;

    @JsonProperty
    Date from;
    @JsonProperty
    Date lastUpdate;

    @JsonProperty
    Date killTime;


    @JsonProperty
    String esId;

    @JsonProperty
    QueryStatus queryStatus;

    @JsonProperty
    long failCounter;

    @JsonIgnore
    ObjectMapper mapper;

    public ZkillboardQueryEntity() {
        mapper = new ObjectMapper();
    }

//    public ZkillboardQueryEntity(long id, long killID, String type,Date to) {
//        super();
//        this.id = id;
//        this.killID = killID;
//        this.type = type;
//        this.to = to;
//    }


    public Date getKillTime() {
        return killTime;
    }

    public void setKillTime(Date killTime) {
        this.killTime = killTime;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEsId() {
        return esId;
    }

    public void setEsId(String esId) {
        this.esId = esId;
    }

    public long getKillID() {
        return killID;
    }

    public void setKillID(long killID) {
        this.killID = killID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getFailCounter() {
        return failCounter;
    }

    public void setFailCounter(long failCounter) {
        this.failCounter = failCounter;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public QueryStatus getQueryStatus() {
        return queryStatus;
    }

    public void setQueryStatus(QueryStatus queryStatus) {
        this.queryStatus = queryStatus;
    }

    public String getQueryList() {
        return queryList;
    }

    public void setQueryList(String queryList) {
        this.queryList = queryList;
    }

    public void setEsc(ElasticSearchClient esc) {
        this.esc = esc;
    }


    public String getName() {
        return name;
    }

    public void generateName(Zkilloard kill) {
        if (this.name == null) {
            if (type.equals("allianceID"))
                setName(kill.getVictim().getAllianceName());
            if (type.equals("corporationID"))
                setName(kill.getVictim().getCorporationName());
            if (type.equals("characterID"))
                setName(kill.getVictim().getCharacterName());
            if (type.equals("groupID"))
                setName(kill.getVictim().getShipTypeID() + "-group");
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return type + ": " + id + " KillID :" + killID + " KillTime: "+killTime+" status: " + this.queryStatus.toString();
    }

    @JsonIgnore
    public String getJson() {
        try {
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @JsonIgnore
    public void save() throws JsonProcessingException {
        esc.insert(mapper.writeValueAsString(this), TYPE);
    }

    @JsonIgnore
    public void update() throws JsonProcessingException {
        if (this.lastUpdate.getTime() < new Date().getTime())
            this.setLastUpdate(new Date());

        if (this.getEsId() != null) {
            esc.update(getJson(), TYPE, this.getEsId());
        } else {
            log.error("no es");
        }
    }

    @JsonIgnore
    public void update(Date nextUpdate) throws JsonProcessingException {
        this.setLastUpdate(nextUpdate);

        if (this.getEsId() != null) {
            esc.update(getJson(), TYPE, this.getEsId());
        } else {
            log.error("no es");
        }
    }

    @JsonIgnore
    public void addToQuery() throws JsonProcessingException {
        this.setQueryStatus(QueryStatus.WAITING);
        this.update();
    }
}
