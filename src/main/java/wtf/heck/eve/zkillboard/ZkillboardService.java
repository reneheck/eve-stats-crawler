package wtf.heck.eve.zkillboard;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wtf.heck.elasticsearch.ElasticSearchClient;
import wtf.heck.eve.zkillboard.json.Zkilloard;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by cerias on 10.04.15.
 */
@RequestScoped
public class ZkillboardService {
    Logger log = LoggerFactory.getLogger(ZkillboardService.class);

    @Inject
    ElasticSearchClient esc;

    ObjectMapper mapper = new ObjectMapper();

    public void saveKills(ArrayList<Zkilloard> kills) {
        BulkRequestBuilder bulkRequest = esc.getClient().prepareBulk();
        for (Zkilloard kill : kills) {
            try {
                bulkRequest.add(esc.getClient().prepareIndex(esc.getINDEX(), "zkillboard", kill.getKillID() + "").setSource(mapper.writeValueAsString(kill)));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        BulkResponse responses = bulkRequest.execute().actionGet();

        if (responses.hasFailures()) {
            log.error(responses.buildFailureMessage());
        }
    }

    public ZkillboardQueryEntity insert(ZkillboardQueryEntity queryEntity, Zkilloard[] killsArray) throws JsonProcessingException {

        queryEntity.setEsc(esc);

        ArrayList<Zkilloard> kills = new ArrayList<Zkilloard>();


        for (Zkilloard z : killsArray)
            kills.add(z);

        if (kills.size() > 0) {
            saveKills(kills);
            int killListID = 0;
            if (queryEntity.getQueryList().contains("forward")) {
                killListID = kills.size() - 1;
            }

            queryEntity.setKillID(kills.get(killListID).getKillID());
            queryEntity.generateName(kills.get(killListID));

            if (kills.get(killListID).getKillTime().getTime() < (new Date().getTime() - ((long) 1000 * 60 * 60 * 12))) {
                queryEntity.setQueryStatus(QueryStatus.WAITING);
                log.info("Query success with: "+kills.size()+" should be waiting -> " + queryEntity.toString());


            } else {
                queryEntity.setQueryStatus(QueryStatus.DONE);
                log.info("Query success should be done " + queryEntity.toString());

            }

        } else {
            log.info("nothing? \n" + queryEntity);
            queryEntity.setFailCounter(queryEntity.failCounter + 1);


            if (queryEntity.getFailCounter() > 3) {
                queryEntity.setQueryStatus(QueryStatus.ERROR);
                log.error(queryEntity + " too many errors");
            } else {
                queryEntity.setQueryStatus(QueryStatus.WAITING);
                Date d = new Date();
                d.setTime(d.getTime() + (long) 1000 * 60 * 60 * 1);
                queryEntity.setLastUpdate(d);
            }
        }

        queryEntity.update();
        return queryEntity;
    }


}
