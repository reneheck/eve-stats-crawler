package wtf.heck.eve.zkillboard;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import wtf.heck.elasticsearch.ElasticSearchClient;

import javax.ejb.*;
import javax.inject.Inject;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Created by cerias on 28.11.14.
 */
@Stateless
public class ZkillboardWorker {
    Logger log = LoggerFactory.getLogger(ZkillboardWorker.class);

    private ObjectMapper mapper = new ObjectMapper();


    @Inject
    ZkillBoardStatus zkillBoardStatus;

    @Inject
    ElasticSearchClient esc;

    private final String TYPE = "requests";
    @EJB
    private ZkillboardParser parser;

    public ZkillboardWorker() {

    }


    @AccessTimeout(value = 120, unit = TimeUnit.SECONDS)
    @Schedule(minute = "*/5", hour = "*")
    private void resetQueryCount() throws IOException, InterruptedException {
        if(zkillBoardStatus.getRequest()>=355) {
            zkillBoardStatus.setMaxCount(zkillBoardStatus.getMaxCount()+1);
            if(zkillBoardStatus.getMaxCount()>2){
                zkillBoardStatus.setRequest(1);
            }
        }
    }

    @AccessTimeout(value = 120, unit = TimeUnit.SECONDS)
    @Schedule(second = "*/10",minute = "*", hour = "*")
    private void work() throws IOException, InterruptedException {

//        if(zkillBoardStatus.getRequest()<355) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.HOUR, -24);

            BoolQueryBuilder queryBuilder = new BoolQueryBuilder();
            queryBuilder.must(QueryBuilders.matchQuery("queryStatus", "WAITING"));
            queryBuilder.must(QueryBuilders.matchQuery("queryList", "forward"));
//        queryBuilder.must(QueryBuilders.matchQuery("type", "groupID"));
//        queryBuilder.must(QueryBuilders.rangeQuery("killTime").lt(cal.getTime().getTime()));
        queryBuilder.must(QueryBuilders.rangeQuery("lastUpdate").lt((new Date().getTime() - (1000 * 60 * 2))));

            SearchRequestBuilder searchRequest = esc.getClient().prepareSearch(esc.getINDEX());
            searchRequest.setTypes(TYPE);
            searchRequest.setSize(1);
            searchRequest.addSort("lastUpdate", SortOrder.ASC);
            searchRequest.setQuery(queryBuilder);

            SearchResponse response = searchRequest.execute().actionGet();



            if (response.getHits().getHits().length > 0) {
                ZkillboardQueryEntity queryEntity;

                queryEntity = mapper.readValue(response.getHits().getAt(0).getSourceAsString(), ZkillboardQueryEntity.class);
                queryEntity.setQueryStatus(QueryStatus.INQUERY);
                queryEntity.setEsc(esc);
                if (queryEntity.getEsId() == null) {
                    queryEntity.setEsId(response.getHits().getAt(0).getId());
                    log.info("Setting ES ID: " + response.getHits().getAt(0).getId());
                }
                queryEntity.update();
                Thread.sleep(2000);
                parser.killsAfter(queryEntity);


            }
//        }
    }

    @Timeout
    @AccessTimeout(value = 120, unit = TimeUnit.SECONDS)
    @Schedule(second = "*/10", minute = "*", hour = "*")
    private void workEs() throws IOException, InterruptedException {

//        if(zkillBoardStatus.getRequest()<355) {

            Calendar cal = Calendar.getInstance();
            cal.set(2010, 1, 1);

            BoolQueryBuilder queryBuilder = new BoolQueryBuilder();
            queryBuilder.must(QueryBuilders.matchQuery("queryStatus", "WAITING"));
            queryBuilder.must(QueryBuilders.matchQuery("queryList", "backward"));
//        queryBuilder.must(QueryBuilders.matchQuery("type", "groupID"));
//            queryBuilder.must(QueryBuilders.rangeQuery("killTime").gt(cal.getTime().getTime()));
        queryBuilder.must(QueryBuilders.rangeQuery("lastUpdate").lt((new Date().getTime() - (1000 * 60 * 2))));

            SearchRequestBuilder searchRequest = esc.getClient().prepareSearch(esc.getINDEX());
            searchRequest.setTypes(TYPE);
            searchRequest.setSize(1);
            searchRequest.addSort("lastUpdate", SortOrder.ASC);
            searchRequest.setQuery(queryBuilder);

            SearchResponse response = searchRequest.execute().actionGet();


            if (response.getHits().getHits().length > 0) {
                ZkillboardQueryEntity queryEntity;

                Random generator = new Random();
                int i = generator.nextInt(response.getHits().getHits().length);
                queryEntity = mapper.readValue(response.getHits().getAt(0).getSourceAsString(), ZkillboardQueryEntity.class);
                queryEntity.setQueryStatus(QueryStatus.INQUERY);
                queryEntity.setEsc(esc);
                if (queryEntity.getEsId() == null) {
                    queryEntity.setEsId(response.getHits().getAt(0).getId());
                    log.info("Setting ES ID: " + response.getHits().getAt(0).getId());
                }
                queryEntity.update();
                Thread.sleep(2000);
                parser.killsBefore(queryEntity);


            }
        }
//    }
}
