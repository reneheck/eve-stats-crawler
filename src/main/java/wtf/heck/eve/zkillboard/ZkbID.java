package wtf.heck.eve.zkillboard;

/**
 * Created by cerias on 11.10.14.
 */
public class ZkbID {
    public long afterKill;
    public long beforeKill;

    public long getAfterKill() {
        return afterKill;
    }

    public void setAfterKill(long afterKill) {
        this.afterKill = afterKill;
    }

    public long getBeforeKill() {
        return beforeKill;
    }

    public void setBeforeKill(long beforeKill) {
        this.beforeKill = beforeKill;
    }

    public ZkbID() {
        beforeKill =91671616;
        afterKill =0;
    }

    public ZkbID(double afterKill, double beforeKill) {
        this.afterKill = (long) afterKill;
        this.beforeKill = (long) beforeKill;
    }
}
