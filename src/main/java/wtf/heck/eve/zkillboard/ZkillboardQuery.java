package wtf.heck.eve.zkillboard;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wtf.heck.elasticsearch.ElasticSearchClient;


import javax.ejb.*;
import javax.inject.Inject;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by cerias on 14.01.15.
 */

public class ZkillboardQuery {
    Logger log = LoggerFactory.getLogger(ZkillboardQuery.class);

    @Inject
    ElasticSearchClient esc;

    private static String TYPE = "requests";
    private ObjectMapper mapper = new ObjectMapper();

    public ZkillboardQuery() {

    }

//    @Timeout
//    @AccessTimeout(value = 28, unit = TimeUnit.SECONDS)
//    @Schedule(second = "*/30", minute = "*", hour = "*")
//    public void fillQuery() throws IOException {
//        log.info("regenerate query list");
//
//        BoolQueryBuilder queryBuilder = new BoolQueryBuilder();
//        queryBuilder.must(QueryBuilders.matchQuery("queryStatus", "WAITING"));
//        queryBuilder.must(QueryBuilders.rangeQuery("lastUpdate").lt((new Date().getTime() - (1000 * 60 * 2))));
//
//        SearchRequestBuilder searchRequest = esc.getClient().prepareSearch(esc.getINDEX());
//        searchRequest.setTypes(TYPE);
//        searchRequest.setSize(100);
//        searchRequest.setQuery(queryBuilder);
//
//
//        SearchResponse response = searchRequest.execute().actionGet();
//        if (response.getHits().totalHits() > 0)
//            log.info("Adding to Query " + response.getHits().getTotalHits() + "...");
//        for (SearchHit hit : response.getHits().getHits()) {
//            ZkillboardQueryEntity zqe = mapper.readValue(hit.getSourceAsString(), ZkillboardQueryEntity.class);
//            zqe.setEsc(esc);
//
//            if (zqe.getEsId() == null) {
//                zqe.setEsId(hit.getId());
//                log.info("Setting ES ID: " + hit.getId());
//            }
//            zqe.update();
//            zqe.addToQuery();
//
//        }
//    }

    @Timeout
    @AccessTimeout(value = 28, unit = TimeUnit.SECONDS)
    @Schedule(minute = "*/5", hour = "*")
    private void regenerateQuery() throws IOException {


        BoolQueryBuilder queryBuilder = new BoolQueryBuilder();
        queryBuilder.must(QueryBuilders.matchQuery("queryStatus", "INQUERY"));
        queryBuilder.must(QueryBuilders.rangeQuery("lastUpdate").lt((new Date().getTime() - (1000 * 60 * 15))));

        SearchRequestBuilder searchRequest = esc.getClient().prepareSearch(esc.getINDEX());
        searchRequest.setTypes(TYPE);
        searchRequest.setSize(100);
        searchRequest.setQuery(queryBuilder);

        SearchResponse response = searchRequest.execute().actionGet();

        for (SearchHit hit : response.getHits().getHits()) {
            ZkillboardQueryEntity zqe = mapper.readValue(hit.getSourceAsString(), ZkillboardQueryEntity.class);
                zqe.setQueryStatus(QueryStatus.WAITING);
                log.warn("Reparing Query: " + zqe.getName());
                zqe.setEsc(esc);

                zqe.update();
            }

        }

}
