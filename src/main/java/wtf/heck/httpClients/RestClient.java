package wtf.heck.httpClients;



import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wtf.heck.eve.zkillboard.ZkillBoardStatus;

import javax.inject.Inject;
import java.io.*;
import java.net.*;
import java.util.zip.GZIPInputStream;

/**
 * Created by cerias on 08.12.14.
 */

public class RestClient {

    ZkillBoardStatus zkillBoardStatus;
    Logger log = LoggerFactory.getLogger(RestClient.class);
    private String urlString;
    private String urlBase;
    private String urlPath ="";
    private String urlParameters = "";
    private String response;

    public RestClient() {

    }

    public RestClient(String urlString) {
        this.urlString = urlString;
        try {
            init();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public RestClient(String urlString,ZkillBoardStatus zkillBoardStatus) {
        this.zkillBoardStatus=zkillBoardStatus;
        this.urlString = urlString;
        try {
            init();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public String getUrlBase() {
        return urlBase;
    }

    public void setUrlBase(String urlBase) {
        this.urlBase = urlBase;
    }

    public String getUrlPath() {
        return urlPath;
    }

    public void setUrlPath(String urlPath) {
        this.urlPath = urlPath;
    }

    public String getUrlParameters() {
        return urlParameters;
    }

    public void setUrlParameters(String urlParameters) {
        this.urlParameters = urlParameters;
    }

    public void appendGetParameter(String name,String value){
        if(urlParameters.equals("")) {
            this.urlParameters = "?" + name + "=" + value;
        }else{
            this.urlParameters = this.urlParameters + "&"+name+"="+value;
        }
    }


    private void init() throws IOException, URISyntaxException {
        URI uri = new URI(urlString);
        HttpGet httpget = new HttpGet(uri);

        httpget.addHeader("Accept-Encoding", "gzip");
        httpget.addHeader("User-Agent","http://eve-stats.net/");
        httpget.addHeader("Maintainer","Javani | reneheck@gmail.com");
        HttpClient httpclient = new DefaultHttpClient();

        final HttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, 20000);
        httpclient = new DefaultHttpClient(httpParams);

        HttpResponse response = httpclient.execute(httpget);
        // check response headers.
        String reasonPhrase = response.getStatusLine().getReasonPhrase();
        int statusCode = response.getStatusLine().getStatusCode();

        log.debug(String.format("statusCode: %d", statusCode));
//        System.out.println(String.format("reasonPhrase: %s", reasonPhrase));


        HttpEntity entity = response.getEntity();
        InputStream content = entity.getContent();

        for(Header header:response.getAllHeaders()){

            if(header.getName().equals("X-Bin-Request-Count")){
                zkillBoardStatus.setRequest(Long.parseLong(header.getValue()));
            }
        }
        // apache IO util
        try {

            GZIPInputStream gzip = new GZIPInputStream(content);

            this.response = IOUtils.toString(gzip, "UTF-8");
        } finally {
            // close http network connection
            content.close();
        }

    }

    public String getResponse() {
        return response;
    }
}