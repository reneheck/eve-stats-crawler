package wtf.heck.httpClients;


import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by cerias on 08.12.14.
 */

public class CrestClient {

    private String urlString;
    private String urlBase;
    private String urlPath ="";
    private String urlParameters = "";
    private String response;

    public CrestClient() {

    }

    public CrestClient(String urlString) {
        this.urlString = urlString;
        try {
            init();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public String getUrlBase() {
        return urlBase;
    }

    public void setUrlBase(String urlBase) {
        this.urlBase = urlBase;
    }

    public String getUrlPath() {
        return urlPath;
    }

    public void setUrlPath(String urlPath) {
        this.urlPath = urlPath;
    }

    public String getUrlParameters() {
        return urlParameters;
    }

    public void setUrlParameters(String urlParameters) {
        this.urlParameters = urlParameters;
    }

    public void appendGetParameter(String name,String value){
        if(urlParameters.equals("")) {
            this.urlParameters = "?" + name + "=" + value;
        }else{
            this.urlParameters = this.urlParameters + "&"+name+"="+value;
        }
    }


    private void init() throws IOException, URISyntaxException {
        URI uri = new URI(urlString);
        HttpGet httpget = new HttpGet(uri);

//        httpget.addHeader("Accept-Encoding", "gzip");
        httpget.addHeader("User-Agent","http://eve-stats.net/");
        httpget.addHeader("Maintainer","Javani | reneheck@gmail.com");
        HttpClient httpclient = new DefaultHttpClient();

        final HttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, 20000);
        httpclient = new DefaultHttpClient(httpParams);

        HttpResponse response = httpclient.execute(httpget);
        // check response headers.
        String reasonPhrase = response.getStatusLine().getReasonPhrase();
        int statusCode = response.getStatusLine().getStatusCode();



        HttpEntity entity = response.getEntity();
        InputStream content = entity.getContent();

        ByteArrayOutputStream baos = new ByteArrayOutputStream(1024 * 1024);

        // apache IO util
        try {


            IOUtils.copy(content, baos);
        } finally {
            // close http network connection
            content.close();
        }

        byte[] bytes = baos.toByteArray();

//        System.out.println("HTML as string:" + new String(bytes));
        this.response= new String(bytes);
    }

    public String getResponse() {
        return response;
    }
}