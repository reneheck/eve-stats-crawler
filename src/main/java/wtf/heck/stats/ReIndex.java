package wtf.heck.stats;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.filefilter.OrFileFilter;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.lucene.search.OrFilter;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.OrFilterBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wtf.heck.PropHolder;
import wtf.heck.elasticsearch.ElasticSearchClient;
import wtf.heck.elasticsearch.ElasticSearchCluster;
import wtf.heck.elasticsearch.ElasticStats;
import wtf.heck.eve.zkillboard.json.Zkilloard;
import wtf.heck.stats.killmail.Killmail;
import wtf.heck.stats.killmail.KillmailFactory;

import wtf.heck.eve.zkillboard.json.ZkilloardReady;

import javax.ejb.AccessTimeout;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;


/**
 * Created by cerias on 19.04.15.
 */
@Stateless
public class ReIndex {

    Logger log = LoggerFactory.getLogger(ReIndex.class);
    @Inject
    ElasticSearchClient esc;
    @Inject
    ElasticSearchCluster cluster;

//    @Inject
//    PropHolder prop;

    final long iVersion = 8;

    final private String source = "zkillboard";
    final private String destination = "evekills-3";


    @Timeout
    @AccessTimeout(value = 60, unit = TimeUnit.SECONDS)
    @Schedule(second = "*/10", minute = "*/1", hour = "*")
    private void regenerateQuery() throws IOException {
        long start = System.currentTimeMillis();
//        if(esc.getRequestTime()<4500) {
        ObjectMapper mapper = new ObjectMapper();
        KillmailFactory factory = new KillmailFactory();


        SearchRequestBuilder rein = esc.getClient().prepareSearch(esc.getINDEX());
        rein.setTypes(source);
        OrFilterBuilder orFilter = new OrFilterBuilder();
        orFilter.add(FilterBuilders.rangeFilter("iVersion").lt(iVersion));
        orFilter.add(FilterBuilders.notFilter(FilterBuilders.existsFilter("iVersion")));
        rein.addSort("killTime", SortOrder.DESC);
        rein.setPostFilter(orFilter);
        rein.setSize(500);

        BulkRequestBuilder bulkRequest = esc.getClient().prepareBulk();
        BulkRequestBuilder bulkRequestCluster = cluster.getClient().prepareBulk();

        SearchResponse response = rein.execute().actionGet();
        if (response.getHits().getHits().length > 1)
            log.info("Updating: " + response.getHits().getHits().length + "|from: " + response.getHits().getTotalHits());
        if (response.getHits().getHits().length >= 500) {
            for (SearchHit hit : response.getHits()) {
                if (hit.getSourceAsString() != null) {
                    ZkilloardReady kill = mapper.readValue(hit.getSourceAsString(), ZkilloardReady.class);


                    bulkRequestCluster = factory.fromZkillboard(kill, iVersion, esc, cluster, bulkRequestCluster, destination);

                    kill.setiVersion(iVersion);

                }
            }


            if (bulkRequestCluster.numberOfActions() > 0) {
                TimeValue timeValue = new TimeValue(60000);
                bulkRequestCluster.setTimeout(timeValue);

                cluster.connect();
                esc.connect();
                BulkResponse responses = bulkRequestCluster.execute().actionGet();


                if (responses.hasFailures()) {
                    log.error(responses.buildFailureMessage());

                } else {
                    for (BulkItemResponse bulkItemResponse : responses.getItems()) {
                        if (!bulkItemResponse.isFailed()) {
                            bulkRequest.add(esc.updateFieldBulk(source, bulkItemResponse.getId() + "", "iVersion", iVersion));
                        }
                    }
                    if (bulkRequest.numberOfActions() > 0) {
                        TimeValue timeValue2 = new TimeValue(60000);
                        bulkRequest.setTimeout(timeValue);
                        BulkResponse responses2 = bulkRequest.execute().actionGet();


                        if (responses.hasFailures()) {
                            log.error(responses.buildFailureMessage());
                        }
                    }
                }


                esc.insert(mapper.writeValueAsString(new ElasticStats("killmail", destination, responses.getTookInMillis(), (long) bulkRequestCluster.numberOfActions())), "indexStats");
                log.info("Transfer time: " + ((System.currentTimeMillis() - start) / 1000) + " ES Time: " + response.getTookInMillis());
            }


        }
    }
}