package wtf.heck.stats.killmail;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import wtf.heck.eve.data.SolarSystem;

import java.util.*;

/**
 * Created by cerias on 22.02.15.
 */
public class Killmail {

    @JsonProperty("killID")
    private long killID;
    @JsonProperty("iVersion")
    private long iVersion;
    @JsonProperty("solarSystem")
    private SolarSystem solarSystem;


    @JsonProperty("killTime")
    private Date killTime;
    @JsonProperty("moonID")
    private String moonID;
    @JsonProperty("victim")
    private Victim victim;
    @JsonProperty("attackers")
    private List<Attacker> attackers = new ArrayList<Attacker>();
    @JsonProperty("items")
    private List<KillmailItem> items = new ArrayList<KillmailItem>();
    @JsonProperty("isk")
    private Isk isk;
    @JsonProperty("source")
    private String source;

    @JsonProperty("warID")
    private long warID;

    @JsonProperty("minerals")
    private ArrayList<Mineral> minerals = new ArrayList<Mineral>();

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();


    /*******************************************************************************************************************
     *
     * CONSTRUCTOR
     *
     ******************************************************************************************************************/

    public Killmail(){

    }



    /*******************************************************************************************************************
     *
     *  GETTER & SETTER
     *
     ******************************************************************************************************************/
    @JsonProperty("killID")
    public long getKillID() {
        return killID;
    }

    @JsonProperty("killID")
    public void setKillID(long killID) {
        this.killID = killID;
    }

    @JsonProperty("warID")
    public long getWarID() {
        return warID;
    }

    @JsonProperty("warID")
    public void setWarID(long warID) {
        this.warID = warID;
    }

    @JsonProperty("iVersion")
    public long getiVersion() {
        return iVersion;
    }

    @JsonProperty("iVersion")
    public void setiVersion(long iVersion) {
        this.iVersion = iVersion;
    }

    @JsonProperty("solarSystem")
    public SolarSystem getsolarSystem() {
        return solarSystem;
    }

    @JsonProperty("solarSystem")
    public void setsolarSystem(SolarSystem solarSystem) {
        this.solarSystem = solarSystem;
    }

    @JsonProperty("killTime")
    public Date getKillTime() {
        return killTime;
    }

    @JsonProperty("killTime")
    public void setKillTime(Date killTime) {
        this.killTime = killTime;
    }

    @JsonProperty("moonID")
    public String getMoonID() {
        return moonID;
    }

    @JsonProperty("moonID")
    public void setMoonID(String moonID) {
        this.moonID = moonID;
    }

    @JsonProperty("victim")
    public Victim getVictim() {
        return victim;
    }

    @JsonProperty("victim")
    public void setVictim(Victim victim) {
        this.victim = victim;
    }

    @JsonProperty("attackers")
    public List<Attacker> getAttackers() {
        return attackers;
    }

    @JsonProperty("attackers")
    public void setAttackers(List<Attacker> attackers) {
        this.attackers = attackers;
    }

    @JsonProperty("items")
    public List<KillmailItem> getItems() {
        return items;
    }

    @JsonProperty("items")
    public void setItems(List<KillmailItem> items) {
        this.items = items;
    }

    @JsonProperty("isk")
    public Isk getIsk() {
        return isk;
    }

    @JsonProperty("isk")
    public void setIsk(Isk isk) {
        this.isk = isk;
    }

    @JsonProperty("source")
    public String getSource() {
        return source;
    }

    @JsonProperty("source")
    public void setSource(String source) {
        this.source = source;
    }

    @JsonProperty("minerals")
    public ArrayList<Mineral> getMinerals() {
        return minerals;
    }

    @JsonProperty("minerals")
    public void setMinerals(ArrayList<Mineral> minerals) {
        this.minerals = minerals;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }




}
