package wtf.heck.stats.killmail;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by cerias on 20.02.15.
 */
public class Mineral {

    long id;
    @JsonProperty("qtyDropped")
    private long qtyDropped;
    @JsonProperty("qtyDestroyed")
    private long qtyDestroyed;



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getQtyDropped() {
        return qtyDropped;
    }

    public void setQtyDropped(long qtyDropped) {
        this.qtyDropped = qtyDropped;
    }

    public long getQtyDestroyed() {
        return qtyDestroyed;
    }

    public void setQtyDestroyed(long qtyDestroyed) {
        this.qtyDestroyed = qtyDestroyed;
    }

    public Mineral() {

    }

    /***
     *
     * @param id
     * @param qtyDestroyed
     */
    public Mineral(long id,long qtyDestroyed) {
        this.id=id;
        this.qtyDestroyed=qtyDestroyed;

    }

}
