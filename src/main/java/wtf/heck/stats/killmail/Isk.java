
package wtf.heck.stats.killmail;

import com.fasterxml.jackson.annotation.*;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "zTotalValue",
    "points",
    "source"
})
public class Isk {

    @JsonProperty("zTotalValue")
    private double zTotalValue;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("zTotalValue")
    public double getzTotalValue() {
        return zTotalValue;
    }

    @JsonProperty("zTotalValue")
    public void setzTotalValue(double zTotalValue) {
        this.zTotalValue = zTotalValue;
    }




    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
