package wtf.heck.stats.killmail;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;

import wtf.heck.elasticsearch.ElasticSearchClient;
import wtf.heck.elasticsearch.ElasticSearchCluster;
import wtf.heck.eve.data.EveItem;
import wtf.heck.eve.data.SolarSystem;
import wtf.heck.eve.zkillboard.json.Zkb;
import wtf.heck.eve.zkillboard.json.Zkilloard;
import wtf.heck.eve.zkillboard.json.ZkilloardReady;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.IOException;
import java.util.*;

/**
 * Created by cerias on 22.02.15.
 */
@ApplicationScoped
public class KillmailFactory {
    Logger log = LoggerFactory.getLogger(KillmailFactory.class);


    ObjectMapper mapper = new ObjectMapper();

    ElasticSearchClient esc;
    ElasticSearchCluster cluster;
    String index;

    HashMap<Long,EveItem> items = new HashMap<>();
    HashMap<Long,SolarSystem> system = new HashMap<>();


    public Killmail fromZkillboard(ZkilloardReady zkilloard,long version,ElasticSearchClient esc) throws IOException {
        this.esc  = esc;
        Killmail killmail = new Killmail();
        killmail.setKillID(zkilloard.getKillID());
        killmail.setiVersion(version);
        killmail.setsolarSystem(solarSystem(zkilloard.getSolarSystemID()));
        killmail.setKillTime(zkilloard.getKillTime());
        killmail.setMoonID(zkilloard.getMoonID());
        if(zkilloard.getVictim()!=null)
        killmail.setVictim(zVictim(zkilloard.getVictim()));
        if(zkilloard.getAttackers()!=null)
        killmail.setAttackers(zAttackers(zkilloard.getAttackers(),zkilloard.getVictim().getDamageTaken(),zkilloard.getZkb().getTotalValue()));
        if(zkilloard.getItems()!=null)
        killmail.setItems(zItems(zkilloard.getItems()));
        if (zkilloard.getZkb() != null)
            killmail.setSource(zkilloard.getZkb().getSource());
        killmail.setIsk(zIsk(zkilloard.getZkb()));

        killmail.setMinerals(collectMinerals(killmail));
        return killmail;
    }

    public BulkRequestBuilder fromZkillboard(ZkilloardReady zkilloard,long version,ElasticSearchClient esc,ElasticSearchCluster cluster,BulkRequestBuilder bulkRequestBuilder,String index) throws IOException {
        this.esc  = esc;
        this.cluster =cluster;
        this.index=index;
        Killmail killmail = new Killmail();
        killmail.setKillID(zkilloard.getKillID());
        killmail.setiVersion(version);
        killmail.setsolarSystem(solarSystem(zkilloard.getSolarSystemID()));
        killmail.setKillTime(zkilloard.getKillTime());
        killmail.setMoonID(zkilloard.getMoonID());
        if(zkilloard.getVictim()!=null)
            killmail.setVictim(zVictim(zkilloard.getVictim()));


        if (zkilloard.getZkb() != null)
            killmail.setSource(zkilloard.getZkb().getSource());
        killmail.setIsk(zIsk(zkilloard.getZkb()));

        killmail.setMinerals(collectMinerals(killmail));

        bulkRequestBuilder.add(cluster.getClient().prepareIndex(index,"killmail",killmail.getKillID()+"").setSource(mapper.writeValueAsString(killmail)));

        if(zkilloard.getAttackers()!=null) {
            bulkRequestBuilder = zAttackers(zkilloard.getAttackers(), zkilloard.getVictim().getDamageTaken(), zkilloard.getZkb().getTotalValue(),bulkRequestBuilder,killmail.getKillID()+"",killmail.getKillTime());
        }

        if(zkilloard.getItems()!=null) {
            bulkRequestBuilder = zItems(zkilloard.getItems(),bulkRequestBuilder,killmail.getKillID()+"");
        }

        return bulkRequestBuilder;
    }



    private Victim zVictim(wtf.heck.eve.zkillboard.json.Victim zVictim) throws IOException {
        Victim victimTmp = new Victim();
        victimTmp.setAllianceID(zVictim.getAllianceID());
        if(!zVictim.getAllianceName().equals(""))
        victimTmp.setAllianceName(zVictim.getAllianceName());
        victimTmp.setCharacterID(zVictim.getCharacterID());
        if(!zVictim.getCharacterName().equals(""))
        victimTmp.setCharacterName(zVictim.getCharacterName());
        victimTmp.setCorporationID(zVictim.getCorporationID());
        if(!zVictim.getCharacterName().equals(""))
        victimTmp.setCorporationName(zVictim.getCorporationName());
        victimTmp.setDamageTaken(zVictim.getDamageTaken());
        victimTmp.setFactionID(zVictim.getFactionID());
        if(!zVictim.getFactionName().equals(""))
        victimTmp.setFactionName(zVictim.getFactionName());
        victimTmp.setshipType(getItem(zVictim.getShipTypeID()));
        return victimTmp;
    }

    private List<Attacker> zAttackers(List<wtf.heck.eve.zkillboard.json.Attacker> zAttacker,double damageTaken,double value) throws IOException {
        List<Attacker> attackers = new ArrayList<>();

        for (wtf.heck.eve.zkillboard.json.Attacker z : zAttacker) {
            Attacker attacker = new Attacker();
            attacker.setshipType(getItem(z.getShipTypeID()));
            attacker.setweaponType(getItem(z.getWeaponTypeID()));
            attacker.setCorporationID(z.getCorporationID());
            if(!z.getCorporationName().equals(""))
            attacker.setCorporationName(z.getCorporationName());
            if(!z.getCharacterName().equals(""))
            attacker.setCharacterName(z.getCharacterName());
            attacker.setCharacterID(z.getCharacterID());
            if(!z.getAllianceName().equals(""))
            attacker.setAllianceName(z.getAllianceName());
            attacker.setAllianceID(z.getAllianceID());
            if(!z.getFactionName().equals(""))
            attacker.setFactionName(z.getFactionName());
            attacker.setFactionID(z.getFactionID());
            attacker.setFinalBlow(z.getFinalBlow());
            attacker.setDamageDone(z.getDamageDone());
            attacker.setSecurityStatus(z.getSecurityStatus());


            attacker.setIskPerDamage(
                    ( (100/damageTaken)*z.getDamageDone()*value)
            );


            attackers.add(attacker);
        }
        return attackers;
    }

    private BulkRequestBuilder zAttackers(List<wtf.heck.eve.zkillboard.json.Attacker> zAttacker,double damageTaken,double value,BulkRequestBuilder bulkRequestBuilder,String parent,Date time) throws IOException {


        for (wtf.heck.eve.zkillboard.json.Attacker z : zAttacker) {
            Attacker attacker = new Attacker();
            attacker.setshipType(getItem(z.getShipTypeID()));
            attacker.setweaponType(getItem(z.getWeaponTypeID()));
            attacker.setCorporationID(z.getCorporationID());
            if(!z.getCorporationName().equals(""))
                attacker.setCorporationName(z.getCorporationName());
            if(!z.getCharacterName().equals(""))
                attacker.setCharacterName(z.getCharacterName());
            attacker.setCharacterID(z.getCharacterID());
            if(!z.getAllianceName().equals(""))
                attacker.setAllianceName(z.getAllianceName());
            attacker.setAllianceID(z.getAllianceID());
            if(!z.getFactionName().equals(""))
                attacker.setFactionName(z.getFactionName());
            attacker.setFactionID(z.getFactionID());
            attacker.setFinalBlow(z.getFinalBlow());
            attacker.setDamageDone(z.getDamageDone());
            attacker.setKillTime(time);
            attacker.setSecurityStatus(z.getSecurityStatus());


            attacker.setIskPerDamage(
                    ( (100/damageTaken)*z.getDamageDone()*value)
            );


            bulkRequestBuilder.add(cluster.getClient().prepareIndex(index,"attackers").setParent(parent).setSource(mapper.writeValueAsString(attacker)));
        }

        return bulkRequestBuilder;
    }

    private SolarSystem solarSystem(long id) throws IOException {
        SolarSystem solarSystem = null;
        if(system.containsKey(id)){
            solarSystem = system.get(id);
        }else{
            Object s = esc.get("solarSystem",id+"",SolarSystem.class);
            if(s instanceof SolarSystem) {
                solarSystem = (SolarSystem) esc.get("solarSystem", id + "", SolarSystem.class);
                system.put(id,solarSystem);
            }
        }

        return solarSystem;

    }
    private List<KillmailItem> zItems(List<wtf.heck.eve.zkillboard.json.Item> zitems) throws IOException {
        List<KillmailItem> items = new ArrayList<>();
        for (wtf.heck.eve.zkillboard.json.Item i : zitems) {
            KillmailItem item = new KillmailItem();
            item.setFlag(i.getFlag());
            item.setQtyDestroyed(i.getQtyDestroyed());
            item.setQtyDropped(i.getQtyDropped());
            item.setSingleton(i.getSingleton());
            item.settype(getItem(i.getTypeID()));

            items.add(item);
        }
        return items;
    }

    private BulkRequestBuilder zItems(List<wtf.heck.eve.zkillboard.json.Item> zitems,BulkRequestBuilder bulkRequestBuilder,String parent) throws IOException {

        for (wtf.heck.eve.zkillboard.json.Item i : zitems) {
            KillmailItem item = new KillmailItem();
            item.setFlag(i.getFlag());
            item.setQtyDestroyed(i.getQtyDestroyed());
            item.setQtyDropped(i.getQtyDropped());
            item.setSingleton(i.getSingleton());
            item.settype(getItem(i.getTypeID()));

            bulkRequestBuilder.add(cluster.getClient().prepareIndex(index,"items").setParent(parent).setSource(mapper.writeValueAsString(item)));
        }
        return bulkRequestBuilder;
    }

    private Isk zIsk(Zkb zkb) {
        Isk isk = new Isk();
        if (zkb != null)
            isk.setzTotalValue(zkb.getTotalValue());
        return isk;
    }

    private EveItem getItem(long id) throws IOException {
        EveItem item;
        if(items.containsKey(id)) {
            item = items.get(id);
        }else{
            item = (EveItem) esc.get("items", id + "", EveItem.class);
            items.put(id,item);
        }
        if(item==null){
            item = new EveItem(id);
        }

        return item;
    }

    private ArrayList<Mineral> collectMinerals(Killmail killmail) throws IOException {
        ArrayList<Mineral> minerals = new ArrayList<>();
        Map<Long,Mineral> mineralMap = new HashMap<>();
        EveItem ei = null;

        if(killmail.getVictim()!=null&&killmail.getVictim().getshipType()!=null) {
            ei = getItem(killmail.getVictim().getshipType().getId());
        }
        if(ei!=null&&ei.getMinerals().size()>0)
            mineralMap = buildMineralMap(mineralMap,ei,0,1);

        for(KillmailItem kmi:killmail.getItems()){
            if(kmi.gettype()!=null){
                ei = getItem(kmi.gettype().getId());
                if(ei!=null) {
                    mineralMap = buildMineralMap(mineralMap, ei, kmi.getQtyDropped(), kmi.getQtyDestroyed());
                }
            }

        }
        for(long mid:mineralMap.keySet())
            minerals.add(mineralMap.get(mid));

        return minerals;
    }

    private Map<Long, Mineral> buildMineralMap(Map<Long, Mineral> mineralMap, EveItem ei, long dropped, long destroyed) {
        for (wtf.heck.eve.data.Mineral mineral : ei.getMinerals()) {
            Mineral min;
            min = mineralMap.get(mineral.getId());
            if (min == null) {
                min = new Mineral();
                min.setId(mineral.getId());
                min.setQtyDestroyed(mineral.getQuantity() * destroyed);
                min.setQtyDropped(mineral.getQuantity() * dropped);
                mineralMap.put(mineral.getId(), min);
            } else {
                min.setQtyDropped(min.getQtyDropped() + (mineral.getQuantity() * dropped));
                min.setQtyDestroyed(min.getQtyDestroyed() + (mineral.getQuantity() * destroyed));
                mineralMap.remove(mineral.getId());
                mineralMap.put(mineral.getId(), min);
            }
        }
        return mineralMap;
    }
}
