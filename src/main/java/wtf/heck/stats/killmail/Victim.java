
package wtf.heck.stats.killmail;

import com.fasterxml.jackson.annotation.*;
import wtf.heck.eve.data.EveItem;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "shipType",
    "damageTaken",
    "factionName",
    "factionID",
    "allianceName",
    "allianceID",
    "corporationName",
    "corporationID",
    "characterName",
    "characterID",
    "victim"
})
public class Victim {

    @JsonProperty("shipType")
    private EveItem shipType;

    @JsonProperty("shipTypeID")
    private long shipTypeID;

    @JsonProperty("damageTaken")
    private double damageTaken;
    @JsonProperty("factionName")
    private String factionName;
    @JsonProperty("factionID")
    private long factionID;
    @JsonProperty("allianceName")
    private String allianceName;
    @JsonProperty("allianceID")
    private long allianceID;
    @JsonProperty("corporationName")
    private String corporationName;
    @JsonProperty("corporationID")
    private long corporationID;
    @JsonProperty("characterName")
    private String characterName;
    @JsonProperty("characterID")
    private long characterID;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("shipType")
    public EveItem getshipType() {
        return shipType;
    }

    @JsonProperty("shipType")
    public void setshipType(EveItem shipType) {
        this.shipType = shipType;
    }

    @JsonProperty("damageTaken")
    public double getDamageTaken() {
        return damageTaken;
    }

    @JsonProperty("damageTaken")
    public void setDamageTaken(double damageTaken) {
        this.damageTaken = damageTaken;
    }

    @JsonProperty("factionName")
    public String getFactionName() {
        return factionName;
    }

    @JsonProperty("factionName")
    public void setFactionName(String factionName) {
        this.factionName = factionName;
    }

    @JsonProperty("factionID")
    public long getFactionID() {
        return factionID;
    }

    @JsonProperty("factionID")
    public void setFactionID(long factionID) {
        this.factionID = factionID;
    }

    @JsonProperty("allianceName")
    public String getAllianceName() {
        return allianceName;
    }

    @JsonProperty("allianceName")
    public void setAllianceName(String allianceName) {
        this.allianceName = allianceName;
    }

    @JsonProperty("allianceID")
    public long getAllianceID() {
        return allianceID;
    }

    @JsonProperty("allianceID")
    public void setAllianceID(long allianceID) {
        this.allianceID = allianceID;
    }

    @JsonProperty("corporationName")
    public String getCorporationName() {
        return corporationName;
    }

    @JsonProperty("corporationName")
    public void setCorporationName(String corporationName) {
        this.corporationName = corporationName;
    }

    @JsonProperty("corporationID")
    public long getCorporationID() {
        return corporationID;
    }

    @JsonProperty("corporationID")
    public void setCorporationID(long corporationID) {
        this.corporationID = corporationID;
    }

    @JsonProperty("characterName")
    public String getCharacterName() {
        return characterName;
    }

    @JsonProperty("characterName")
    public void setCharacterName(String characterName) {
        this.characterName = characterName;
    }

    @JsonProperty("characterID")
    public long getCharacterID() {
        return characterID;
    }

    @JsonProperty("characterID")
    public void setCharacterID(long characterID) {
        this.characterID = characterID;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
