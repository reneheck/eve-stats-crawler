package wtf.heck.stats.killmail;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import wtf.heck.eve.data.EveItem;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by cerias on 22.02.15.
 */
public class KillmailItem {

    @JsonProperty("type")
    private EveItem type;
    @JsonProperty("flag")
    private long flag;
    @JsonProperty("qtyDropped")
    private long qtyDropped;
    @JsonProperty("qtyDestroyed")
    private long qtyDestroyed;
    @JsonProperty("singleton")
    private String singleton;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("type")
    public EveItem gettype() {
        return type;
    }

    @JsonProperty("type")
    public void settype(EveItem type) {
        this.type = type;
    }

    @JsonProperty("flag")
    public long getFlag() {
        return flag;
    }

    @JsonProperty("flag")
    public void setFlag(long flag) {
        this.flag = flag;
    }

    @JsonProperty("qtyDropped")
    public long getQtyDropped() {
        return qtyDropped;
    }

    @JsonProperty("qtyDropped")
    public void setQtyDropped(long qtyDropped) {
        this.qtyDropped = qtyDropped;
    }

    @JsonProperty("qtyDestroyed")
    public long getQtyDestroyed() {
        return qtyDestroyed;
    }

    @JsonProperty("qtyDestroyed")
    public void setQtyDestroyed(long qtyDestroyed) {
        this.qtyDestroyed = qtyDestroyed;
    }

    @JsonProperty("singleton")
    public String getSingleton() {
        return singleton;
    }

    @JsonProperty("singleton")
    public void setSingleton(String singleton) {
        this.singleton = singleton;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
