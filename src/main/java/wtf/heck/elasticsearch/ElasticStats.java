package wtf.heck.elasticsearch;

import java.util.Date;

/**
 * Created by cerias on 24.05.15.
 */
public class ElasticStats {

    String type;
    String index;
    long time;
    long request;
    Date timestamp;

    public ElasticStats(String type, String index, long time, long request) {
        this.type = type;
        this.index = index;
        this.time = time;
        this.request = request;
        this.timestamp = new Date();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public long getRequest() {
        return request;
    }

    public void setRequest(long request) {
        this.request = request;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
}
