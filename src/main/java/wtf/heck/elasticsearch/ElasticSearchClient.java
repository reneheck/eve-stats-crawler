package wtf.heck.elasticsearch;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequestBuilder;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.get.MultiGetRequestBuilder;
import org.elasticsearch.action.get.MultiGetResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateRequestBuilder;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

import org.elasticsearch.common.unit.TimeValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;
import java.io.IOException;

/**
 * Created by cerias on 07.06.14.
 */
@ApplicationScoped
public class ElasticSearchClient {

    Logger log = LoggerFactory.getLogger(ElasticSearchClient.class);



    private String INDEX  = "eve";
    TransportClient client;

    long requestTime;
    Settings settings;

    ObjectMapper mapper = new ObjectMapper();

    public ElasticSearchClient(){
        log.error("ES Starting...");
        settings = ImmutableSettings.settingsBuilder()
                .put("cluster.name", "eve")
                .put("transport.tcp.connect_timeout","60")
                .build();
        this.client = new TransportClient(settings)
                .addTransportAddress(new InetSocketTransportAddress("localhost", 9300));

    }

    public void connect(){
        if(client==null||client.connectedNodes().size()==0) {
            if (client != null)
                client.close();

            this.client = new TransportClient(settings)
                    .addTransportAddress(new InetSocketTransportAddress("localhost", 9300));
        }
    }

    @PreDestroy
    private void shutdown() {
        this.client.close();
        System.out.println("ES Stopping...");
    }

    public long getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(long requestTime) {
        this.requestTime = requestTime;
    }

    public void setClient(TransportClient client) {
        this.client = client;
    }

    public void insert(String json,String type,String id){


        IndexResponse response = client.prepareIndex(INDEX,type, id)
                .setSource(json)
                .execute()
                .actionGet();

        if(!response.isCreated())
            log.info("Type: "+ type + " id: "+ id + " "+response.isCreated());

    }



    public void insert(String json,String type){


        IndexResponse response = client.prepareIndex(INDEX,type)
                .setSource(json)
                .execute()
                .actionGet();

        if(!response.isCreated())
            log.info("Type: "+ type + " id: "+ response.getId() + ""+response.isCreated());

    }

    public boolean exists(String type, String id) throws IOException {



        GetRequestBuilder getRequestBuilder = client.prepareGet();
        getRequestBuilder.setIndex(INDEX);
        getRequestBuilder.setType(type);
        getRequestBuilder.setId(id);

        GetResponse getResponse = getRequestBuilder.execute().actionGet();

        return getResponse.isExists();
    }

    public Object get(String type,String id,Class clazz) throws IOException {


        GetRequestBuilder getRequestBuilder = client.prepareGet();
        getRequestBuilder.setIndex(INDEX);
        getRequestBuilder.setType(type);
        getRequestBuilder.setId(id);

        GetResponse getResponse = getRequestBuilder.execute().actionGet();

        if(getResponse.isExists()) {

            return mapper.readValue(getResponse.getSourceAsString(), clazz);
        }else {
//            log.warn(type+ " not found with ID:"+ id);
            return null;
        }
    }

    public void update(String json,String type,String id){

        DeleteResponse deleteResponse = client.prepareDelete(INDEX,type,id)
                .execute()
                .actionGet();

        IndexResponse response = client.prepareIndex(INDEX,type, id)
                .setSource(json)
                .execute()
                .actionGet();


    }

    public void updateField(String type,String id,String field,Object value){
        UpdateRequestBuilder request = client.prepareUpdate();
        request.setId(id);
        request.setType(type);
        request.setIndex(this.INDEX);
        request.setDoc(field,value);
        UpdateResponse response = request.execute().actionGet();
        if(!response.isCreated())
            log.debug("es update field not done");
    }

    public UpdateRequestBuilder updateFieldBulk(String type,String id,String field,Object value){
        UpdateRequestBuilder request = client.prepareUpdate();
        request.setId(id);
        request.setType(type);
        request.setIndex(this.INDEX);
        request.setDoc(field,value);


        return request;
    }

    public UpdateRequestBuilder updateFieldBulk(String index,String type,String id,String field,Object value){
        UpdateRequestBuilder request = client.prepareUpdate();
        request.setId(id);
        request.setType(type);
        request.setIndex(index);
        request.setDoc(field,value);

        return request;
    }



    public Client getClient(){
        return client;
    }

    public String getINDEX(){
        return INDEX;
    }

    public void bulkInsert(BulkRequestBuilder bulkRequest) {

        BulkResponse bulkResponse = bulkRequest.execute().actionGet();

        if (bulkResponse.hasFailures()) {
           log.error(bulkResponse.buildFailureMessage());
        }

    }




    /**
     *
     * @param bulkRequest
     * @param json
     * @param type
     * @param id
     * @return
     */
    public BulkRequestBuilder bulkEntry(BulkRequestBuilder bulkRequest, String json,String type,String id) {


        bulkRequest.add(client.prepareIndex(INDEX, type, id)
                        .setSource(json)
        );

        return bulkRequest;

    }




}
